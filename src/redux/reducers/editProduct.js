import { EDIT_PRODUCT_PAGE, TOGGLE_SUCCESS_DELETION, TOGGLE_SUCCESS_EDIT, TOGGLE_UNSUCCESS_DELETION, TOGGLE_UNSUCCESS_EDIT, TOGGLE_UNSUCCESS_ADD, TOGGLE_SUCCESS_ADD } from '../types';
import { fade } from '@material-ui/core/styles';

const initialState = {
  edit: false,
  successDeletion: false,
  successEdit: false,
  unsuccessDeletion: false,
  unsuccessEdit: false,
  successAdd: false,
  unsuccessAdd: false

}

const editProduct = (state =initialState, action) => {
  let newState = {...state};
  switch(action.type) {
    case EDIT_PRODUCT_PAGE: 
      newState.edit = !state.edit;
      return newState;
    case TOGGLE_SUCCESS_DELETION:
      newState.successDeletion = action.successDeletion;
      return newState;
    case TOGGLE_SUCCESS_EDIT:
      newState.successEdit = action.successEdit;
      return newState;
      case TOGGLE_UNSUCCESS_DELETION:
      newState.unsuccessDeletion = action.unsuccessDeletion;
      return newState;
    case TOGGLE_UNSUCCESS_EDIT:
      newState.unsuccessEdit = action.unsuccessEdit;
      return newState;
    case TOGGLE_SUCCESS_ADD:
      newState.successAdd = action.successAdd
      return newState;
    case TOGGLE_UNSUCCESS_ADD:
      newState.unsuccessAdd = action.unsuccessAdd;
      return newState;
    default:
      return state;
  }
}

export default editProduct;