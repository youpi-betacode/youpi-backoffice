import { TOGGLE_MODAL_IMPORT_CONTACTS, LOAD_CONTACTS, SET_CONTACTS_PAGE, SET_CONTACTS_PAGE_SIZE, CHECK_VALID_FILE, CHECK_SUCCESS_UPLOAD, SET_FILE_ERROR, SET_CONTACTS_ORDER_BY, TOGGLE_CONTACTS_DESC, SET_CONTACTS_DESC } from '../types'

const initialState = {
  modal: false,
  validFile: true,
  successUpload: false,
  contacts: [],
  page: 1,
  pageSize: 25,
  count: 0,
  error: '',
  orderBy: '',
  desc: false,
  hover: false,
  labels: [
    {
      key: 'email',
      value: 'EMAIL'
    },
    {
      key: 'first_name',
      value: 'NOME'
    },
    {
      key: 'phone_number',
      value: 'TELEFONE'
    },
    {
      key: 'segment',
      value: 'SEGMENTO'
    },
    {
      key: 'status',
      value: 'STATUS'
    },
  ]
}

const maillingList = (state = initialState, action) => {
  let newState = {...state};

  switch(action.type) {
    case TOGGLE_MODAL_IMPORT_CONTACTS:
      newState.modal = !state.modal;
      return newState;
    case LOAD_CONTACTS:
      newState.contacts = action.contacts;
      newState.count = action.count;
      newState.page = action.page;
      newState.pageSize = action.pageSize;
      return newState;
    case SET_CONTACTS_PAGE:
      newState.page = action.page;
      return newState;
    case SET_CONTACTS_PAGE_SIZE:
      newState.pageSize = action.pageSize;
      newState.page = 1;
      return newState;
    case CHECK_VALID_FILE:
      newState.validFile = action.validFile;
      return newState;
    case CHECK_SUCCESS_UPLOAD:
      newState.successUpload = action.successUpload;
      return newState;
    case SET_FILE_ERROR:
      newState.error = action.error;
      return newState;
    case SET_CONTACTS_ORDER_BY:
      newState.orderBy = action.orderBy;
      return newState;
    case TOGGLE_CONTACTS_DESC:
      newState.desc = newState.desc === '' ? false : '';
      return newState;
    case SET_CONTACTS_DESC: 
      newState.desc = false;
      return newState;
    default:
      return state;
  }
}

export default maillingList;