import { LOGIN_EMAIL, LOGIN_PASSWORD} from '../types';

const initialState = {
    email: '',
    password: ''
}

const login = (state = initialState, action) => {
    let newState = { ...state };
    switch (action.type) {
        case LOGIN_EMAIL:
            newState.email = action.email;
            return newState;
        case LOGIN_PASSWORD:
            newState.password = action.password;
            return newState;
        default:
            return state
    }
};

export default login;