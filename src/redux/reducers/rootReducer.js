import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import drawer from './drawer';
import login from './login';
import tablePagination from './tablePagination';
import user from './user';
import stock from './stock';
import editProduct from './editProduct';
import product from './product';
import maillingList from './maillingList';
import loading from './loading';
import categoryGraph from './categoryGraph';
import dashboard from './dashboard';
import variant from './variant';
import youpers from './youpers';

const rootReducer = (history) => combineReducers({
    drawer: drawer,
    login: login,
    router: connectRouter(history),
    stock: stock,
    tablePagination: tablePagination,
    user: user,
    editProduct: editProduct,
    product: product,
    maillingList: maillingList,
    loading: loading,
    categoryGraph,
    dashboard,
    variant,
    youpers
})

export default rootReducer;