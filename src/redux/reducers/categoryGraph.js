import * as types from '../types';

const initialState = {
    filter: 'WEEK'  //filter can be WEEK, MONTH or YEAR
}

const categoryGraph = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_CATEGORY_FILTER:
            let newState = { ...state };
            newState.filter = action.filter;
            return newState;
        default:
            return state;
    }
};

export default categoryGraph;