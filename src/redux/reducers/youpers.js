import { GET_YOUPERS, SET_YOUPERS_PAGE, SET_YOUPERS_PAGE_SIZE, SET_YOUPERS_ORDER_BY, TOGGLE_YOUPERS_DESC, SET_YOUPERS_DESC } from '../types';

const initialState = {
  costumers: [],
  page: 1,
  pageSize: 25,
  orderBy: '',
  desc: false,
  count: 0,
  labels: [
    {
      key: 'id',
      value: 'ID'
    },
    {
      key: 'extra.name',
      value: 'NOME'
    },
    {
      key: 'email',
      value: 'EMAIL'
    },
    {
      key: 'local',
      value: 'LOCALIDADE'
    },
    {
      key: 'contact',
      value: 'TELEFONE'
    },
    {
      key: 'volume',
      value: 'VOLUME DE COMPRAS'
    },
    {
      key: 'total_spent',
      value: 'TOTAL DE COMPRAS €'
    },
  ]
}

const youpers = (state = initialState, action) => {
  let newState = {...state};
  
  switch(action.type) {
    case GET_YOUPERS:
      newState.costumers = action.costumers;
      newState.count = action.count;
      return newState;
    case SET_YOUPERS_PAGE:
      newState.page = action.page;
      return newState;
    case SET_YOUPERS_PAGE_SIZE:
      newState.pageSize = action.pageSize;
      return newState;
    case SET_YOUPERS_ORDER_BY:
      newState.orderBy = action.orderBy;
      return newState;
    case TOGGLE_YOUPERS_DESC:
      newState.desc = newState.desc === '' ? false : '';
      return newState;
    case SET_YOUPERS_DESC:
      newState.desc = false;
      return newState;
    default:
      return newState;
  }
}

export default youpers;