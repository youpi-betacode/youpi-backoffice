import { SET_LOADING } from '../types';

const initialState = {
  loading: false
}

const loading = (state = initialState, action) => {
  let newState = {...state}

  switch(action.type){
    case SET_LOADING:
      newState.loading = action.loading;
      return newState;
    default:
      return state
  }
}

export default loading;