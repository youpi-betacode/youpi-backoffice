import * as types from '../types';

const initialState = {
  channelData: [],
  categoryData: [],
  categoryTotal: 0,
  sponsorData: [],
  productData: null,
}

const dashboard = (state = initialState, action) => {
  let newState = { ...state };
  switch (action.type) {
    case types.SET_DASHBOARD_CHANNEL:
      newState.channelData = mapChannelData(action.channelData);
      return newState;
    case types.SET_DASHBOARD_CATEGORY: 
      newState.categoryData = mapCategoryData(action.categoryData);
      newState.categoryTotal = action.categoryData["Total"];
      return newState;   
    case types.SET_DASHBOARD_SPONSOR:
      newState.sponsorData = mapSponsorData(action.sponsorData);
      return newState; 
    case types.SET_DASHBOARD_PRODUCT:
      newState.productData = mapProductData(action.productData);
      return newState;    
    default:
      return state;
  }
};

function mapChannelData(data){
  let channelData = [];

  for(var channel in data){
    channelData.push({
      name: channel,
      Volume: data[channel].volume,
      Valor: data[channel].total
    })
  }
  return channelData;
}

function mapCategoryData(data){
  let categoryData = [];

  for(var category in data){
    if(category !== "Total") {    
      categoryData.push({
        name: category,
        value: data[category].total_sold
      })
    } 
   
  }

  return categoryData;
}

function mapSponsorData(data){
  let sponsorData = [];

  for(var sponsor in data){
    sponsorData.push({
      name: sponsor,
      data: getSponsorGraphData(data[sponsor])
    })

  }

  return sponsorData;
}

function getSponsorGraphData(sponsor){
  let data = [];

  for(var product in sponsor){
    data.push({
      name: product,
      Volume: sponsor[product].volume,
      Valor: sponsor[product].total
    })
  }

  return data;
}

function mapProductData(data){
  let productData = [];

  for(var date in data){
    let timeDate = getDateFormat(date);
    productData.push(Object.assign({}, {name: timeDate}, data[date]))   
  }
  
  return productData.reverse();
}

const months = ["Jan", "Fev", "Mar","Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];

function getDateFormat(date){
  let timeDate = new Date(date);
  let formatted_date = timeDate.getDate() + " " + months[timeDate.getMonth()]

  return formatted_date;
}

export default dashboard;