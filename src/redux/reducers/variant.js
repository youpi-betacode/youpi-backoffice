import { GET_VARIANT, SET_VARIANT_LABEL, SET_VARIANT_PRICE, SET_VARIANT_STOCK, SET_SUCCESS_VARIANT_DELETE, SET_SUCCESS_VARIANT_EDIT, SET_SUCCESS_VARIANT_ADD, SET_UNSUCCESS_VARIANT_ADD, SET_UNSUCCESS_VARIANT_DELETE, SET_UNSUCCESS_VARIANT_EDIT } from '../types';

const initialState = {
  id: 0,
  reference: '',
  label: '',
  price: 0,
  stock: 0,
  stockStatus: 'Esgotado',
  productId: 0,
  successAdd: false,
  unsuccessAdd: false,
  successDelete: false,
  unsuccessDelete: false,
  successEdit: false,
  unsuccessEdit: false
}

const variant = (state = initialState, action) => {
  let newState = { ...state };

  switch (action.type) {
    case GET_VARIANT:
      newState.id = action.id;
      newState.reference = action.reference;
      newState.label = action.label;
      newState.price = action.price;
      newState.stock = action.stock;
      newState.productId = action.productId;
      if (action.stock > 0)
        newState.stockStatus = 'Em Stock';
      return newState;
    case SET_VARIANT_LABEL:
      newState.label = action.label;
      return newState;
    case SET_VARIANT_PRICE:
      newState.price = action.price;
      return newState;
    case SET_VARIANT_STOCK:
      newState.stock = action.stock;
      return newState;
    case SET_SUCCESS_VARIANT_ADD:
      newState.successAdd = action.successAdd;
      return newState;
    case SET_UNSUCCESS_VARIANT_ADD:
      newState.unsuccessAdd = action.unsuccessAdd;
      return newState;
    case SET_SUCCESS_VARIANT_DELETE:
      newState.successDelete = action.successDelete;
      return newState;
    case SET_UNSUCCESS_VARIANT_DELETE:
      newState.unsuccessDelete = action.unsuccessDelete;
      return newState;
    case SET_SUCCESS_VARIANT_EDIT:
      newState.successEdit = action.successEdit;
      return newState;
    case SET_UNSUCCESS_VARIANT_EDIT:
      newState.unsuccessEdit = action.unsuccessEdit;
      return newState;
    default:
      return newState;
  }
}

export default variant;