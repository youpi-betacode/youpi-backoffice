import { SET_USER } from '../types';

const initialState = {
  firstName: '',
  lastName: '',
  email: '',
  store: 0
}

const user = (state = initialState, action) => {
  let newState = { ...state };


  switch (action.type) {
    case SET_USER:
      newState.firstName = action.firstName;
      newState.lastName = action.lastName;
      newState.email = action.email;
      newState.store = action.store;
      return newState;
    default:
      return state;
  }
};

export default user;