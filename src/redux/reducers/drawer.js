import { TOGGLE_DRAWER } from '../types';

const initialState = {
    open: false
}

const drawer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_DRAWER:
            let newState = { ...state };
            newState.open = !state.open;
            return newState;
        default:
            return state
    }
};

export default drawer;