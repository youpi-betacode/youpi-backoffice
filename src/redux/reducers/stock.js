import { SET_PRODUCTS, SET_STOCK_PAGE, SET_STOCK_PAGE_SIZE, SET_STOCK_ORDER_BY, TOGGLE_STOCK_DESC, SET_STOCK_CATEGORY, SET_STOCK_SUPPLIER, SET_STOCK_DESC } from '../types'

const initialState = {
  products: [],
  page: 1,
  pageSize: 25,
  orderBy: '',
  desc: false,
  category: '',
  supplier: 0,
  count: 0,
  hover: true,
  labels: [
    {
      key: 'name',
      value: 'NOME'
    },
    { 
      key: 'id',
      value: 'ID PRODUTO'
    },
    {
      key: 'category',
      value: 'CATEGORIA'
    },
    {
      key: 'supplier.name',
      value: 'FORNECEDOR'
    },
    {
      key: 'last_sale',
      value: 'ÚLTIMA ENCOMENDA'
    },
  ]
};

const stock = (state = initialState, action) => {
  let newState = { ...state };
  switch (action.type) {
    case SET_PRODUCTS:
      newState.products = action.products;
      newState.count = action.count;
      return newState;
    case SET_STOCK_PAGE:
      newState.page = action.page;
      return newState;
    case SET_STOCK_PAGE_SIZE:
      newState.pageSize = action.pageSize;
      newState.page = 1;
      return newState;
    case SET_STOCK_ORDER_BY:
      newState.orderBy = action.orderBy;
      return newState;
    case TOGGLE_STOCK_DESC:
      newState.desc = newState.desc === '' ? false : '';
      return newState;
    case SET_STOCK_DESC: 
      newState.desc = false;
      return newState;
    case SET_STOCK_CATEGORY:
      newState.category = action.category;
      return newState;
    case SET_STOCK_SUPPLIER:
      newState.supplier = action.supplier;
      return newState;
    default:
      return state;
  }
};

export default stock