import {ROWS_PER_PAGE, CURRENT_TABLE_PAGE} from '../types'

const initialState = {
    nrRows: 10,
    currentPage: 0
};

const rows_per_page = (state = initialState, action) => {
    let newState = {...state};
    switch(action.type){
        case ROWS_PER_PAGE:
            newState.nrRows = action.nrRows;
            return newState;
        case CURRENT_TABLE_PAGE:
            newState.currentPage = action.currentPage;
            return newState;
    default:
        return state
    }
};

export default rows_per_page