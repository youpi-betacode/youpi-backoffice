import { GET_PRODUCT, SET_PRODUCT_ID, HAS_PRODUCT, SET_IMAGE_TO_DISPLAY, GET_PRODUCT_VARIANTS, SET_VARIANTS_PAGE, SET_VARIANTS_PAGE_SIZE, SET_VARIANTS_ORDER_BY, TOGGLE_VARIANTS_DESC, SET_STOCK_PAGE, SET_CATEGORY_FILTER, TOGGLE_MODAL_DELETE_PRODUCT, SET_PRODUCT_DESCRIPTION, SET_PRODUCT_NAME, SET_PRODUCT_CATEGORY, GET_ANALYTICS } from '../types'
import { iconsSrc } from '../../constants'

const initialState = {
  category: '',
  description: '',
  id: 0,
  name: '',
  exists: true,
  selectedImage: iconsSrc.dummyPillow,
  variants: [],
  page: 1,
  pageSize: 25,
  orderBy: '',
  desc: false,
  count: 0,
  modal: false,
  store: '',
  supplier: '',
  volume: 0,
  total: 0,
  buyers: 0,
  hover: true,
  labels: [
    {
      key: 'label',
      value: 'NOME'
    },
    {
      key: 'reference',
      value: 'REFERÊNCIA'
    },
    {
      key: 'price_base',
      value: 'PREÇO (€)'
    },
    {
      key: 'stock',
      value: 'STOCK'
    }
  ]
}

const product = (state = initialState, action) => {
  let newState = {...state};
  
  switch(action.type) {
    case GET_PRODUCT:
      newState.category = action.category;
      newState.description = action.description;
      newState.id = action.id;
      newState.name = action.name;
      newState.exists = true;
      newState.store = action.store;
      newState.supplier = action.supplier;
      return newState;
    case SET_PRODUCT_ID:
      newState.id = action.id;
      return newState;
    case HAS_PRODUCT:
      newState.exists = action.exists;
      return newState;
    case SET_IMAGE_TO_DISPLAY:
      newState.selectedImage = action.selectedImage;
      return newState;
    case GET_PRODUCT_VARIANTS:
      newState.variants = action.variants;
      newState.count = action.count;
      newState.page = action.page;
      newState.pageSize = action.pageSize;
      return newState;
    case SET_STOCK_PAGE:
      newState.page = action.page;
      return newState;
    case SET_VARIANTS_PAGE_SIZE:
      newState.pageSize = action.pageSize;
      newState.page = 1;
      return newState;
    case SET_VARIANTS_ORDER_BY:
      newState.orderBy = action.orderBy;
      return newState;
    case TOGGLE_VARIANTS_DESC:
      newState.desc = newState.desc === '' ? false : '';
      return newState;
    case TOGGLE_MODAL_DELETE_PRODUCT:
      newState.modal = action.modal;
      return newState;
    case SET_PRODUCT_NAME:
      newState.name = action.name;
      return newState;
    case SET_PRODUCT_DESCRIPTION:
      newState.description = action.description;
      return newState;
    case SET_PRODUCT_CATEGORY:
      newState.category = action.category;
      return newState;
      case GET_ANALYTICS:
          newState.volume = action.volume;
          newState.total = Math.ceil(action.total);
          newState.buyers = action.buyers
          return newState;
    default:
      return state;
  }
}

export default product;