import { SET_DASHBOARD_CHANNEL, SET_DASHBOARD_CATEGORY, SET_DASHBOARD_SPONSOR, SET_DASHBOARD_PRODUCT } from '../types';
import axios from 'axios';
import { youpiAPI as api } from '../../constants';

export function getChannelGraph() {
  return function (dispatch) {
    const url = `${api}/store/1/graphs/channels`;
    const token = localStorage.getItem('token');

    axios({
      method: 'get',
      url: url,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {

        dispatch({
          type: SET_DASHBOARD_CHANNEL,
          channelData: response.data
        })

      })
      .catch((error) => {
      })
  }
}

export function getCategoryGraph(filter) {
  let timespan =  filter === 'WEEK' ? 7 : filter === 'MONTH' ? 31 : 365

  return function (dispatch) {
    const url = `${api}/store/1/graphs/categories?timespan=${timespan}`;
    const token = localStorage.getItem('token');

    axios({
      method: 'get',
      url: url,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {

        dispatch({
          type: SET_DASHBOARD_CATEGORY,
          categoryData: response.data
        })

      })
      .catch((error) => {
      })
  }
}

export function getSponsorGraph() {

  return function (dispatch) {
    const url = `${api}/store/1/graphs/sponsors`;
    const token = localStorage.getItem('token');

    axios({
      method: 'get',
      url: url,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {

        dispatch({
          type: SET_DASHBOARD_SPONSOR,
          sponsorData: response.data
        })

      })
      .catch((error) => {
      })
  }
}

export function getProductGraph() {

  return function (dispatch) {
    const url = `${api}/store/1/graphs/products?timespan=8`;
    const token = localStorage.getItem('token');

    axios({
      method: 'get',
      url: url,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        dispatch({
          type: SET_DASHBOARD_PRODUCT,
          productData: response.data
        })
      })
      .catch((error) => {
      })
  }
}





