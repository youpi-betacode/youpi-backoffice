import { SET_PRODUCT_ID, GET_PRODUCT, HAS_PRODUCT, SET_IMAGE_TO_DISPLAY, GET_PRODUCT_VARIANTS, SET_VARIANTS_PAGE, SET_VARIANTS_PAGE_SIZE, SET_VARIANTS_ORDER_BY, TOGGLE_VARIANTS_DESC, SET_VARIANTS_DESC, TOGGLE_MODAL_DELETE_PRODUCT, SET_PRODUCT_NAME, SET_PRODUCT_DESCRIPTION, SET_PRODUCT_CATEGORY, GET_ANALYTICS } from '../types';
import axios from 'axios';
import { push } from 'connected-react-router';
import { youpiAPI as api, page, pageSize } from '../../constants';
import { toggleLoading } from './loadingActions';
import { toggleSuccessDeletion, toggleSuccessEdit, toggleUnsuccessDeletion, toggleUnsuccessEdit } from './editProductActions';

export function setCurrentId(id) {
  return function (dispatch) {
    dispatch ({
      type: SET_PRODUCT_ID,
      id
    })
  }
}

export function getProduct(id){
  return function (dispatch){
    const token = localStorage.getItem('token')

    dispatch(toggleLoading(true))

    axios({
      method: 'get',
      url: api + `/product/${id}`,
      headers: { 
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json' 
      }
      
    })
    .then((response) => {
      dispatch({
        type: GET_PRODUCT,
        category: response.data.category,
        description: response.data.description,
        id: response.data.id,
        name: response.data.name,
        store: response.data.store,
        supplier: response.data.supplier
      });
      dispatch(toggleLoading(false));
      dispatch(toggleLoading(false))
    })
    .catch((error) => {
      dispatch({
        type: HAS_PRODUCT,
        exists: false
      })
      dispatch(toggleLoading(false))
    })
  }
}

export function setDisplayedImage(src) {
  return function(dispatch) {
    dispatch ({
      type: SET_IMAGE_TO_DISPLAY,
      selectedImage: src
    })
  }
}

export function getProductVariants(id,page,pageSize,orderBy,desc) {
  return function(dispatch) {

    const url = `${api}/product/${id}/variants?page=${page}&page_size=${pageSize}&order_by=${orderBy}&desc=${desc}`;
    const token = localStorage.getItem('token')

    dispatch(toggleLoading(true))

   axios({
     method: 'get',
     url,
     headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
     },
   })
   .then(response => {
     dispatch({
       type: GET_PRODUCT_VARIANTS,
       variants: response.data.variants,
       count: response.data.count,
       page: page,
       pageSize: pageSize
     })
     dispatch(toggleLoading(false))
   })
   .catch(error => {
    dispatch(toggleLoading(false))
   })
  }
}

export function setPage(page) {
  return function (dispatch) {
    dispatch({
      type: SET_VARIANTS_PAGE,
      page: page
    })
  }
}

export function setPageSize(pageSize) {
  return function(dispatch) {
    dispatch({
      type: SET_VARIANTS_PAGE_SIZE,
      pageSize: pageSize
    })
  }
}

export function setOrder(order){
  return function (dispatch){
    dispatch({
      type: SET_VARIANTS_ORDER_BY,
      orderBy: order
    })
  }
}

export function toggleDesc(){
  return function (dispatch){
    dispatch({
      type: TOGGLE_VARIANTS_DESC,
    })
  }
}

export function setDesc(){
  return function (dispatch){
    dispatch({
      type: SET_VARIANTS_DESC,
    })
  }
}

export function deleteProduct(id){
  return function(dispatch) {
    const url = `${api}/product/${id}`;
    const token = localStorage.getItem('token');

    axios({
      method: 'delete',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
    .then(response => {
      dispatch(push('/stock'))
      dispatch(toggleDeleteProductModal(false));
      dispatch(toggleSuccessDeletion(true))
    })
    .catch(error => {
      dispatch(toggleUnsuccessDeletion(true))
    })
  }
}

export function toggleDeleteProductModal(toggle) {
  return function (dispatch) {
    dispatch({
      type: TOGGLE_MODAL_DELETE_PRODUCT,
      modal: toggle
    })
  }
}

export function setName(name) {
  return function(dispatch) {
    dispatch({
      type: SET_PRODUCT_NAME,
      name
    })
  }
}

export function setDescription(description) {
  return function(dispatch) {
    dispatch({
      type: SET_PRODUCT_DESCRIPTION,
      description
    })
  }
}

export function setCategory(category) {
  return function(dispatch) {
    dispatch({
      type: SET_PRODUCT_CATEGORY,
      category
    })
  }
}

export function saveEdits(formData,id){
  const url = `${api}/product/${id}`;
  const token = localStorage.getItem('token');

  return function(dispatch){
    axios({
      method: 'put',
      url,
      data: formData, 
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }

    })
    .then(response => {
      dispatch(toggleSuccessEdit(true))
    })
    .catch(error => {
      dispatch(toggleUnsuccessEdit(true))
    })
  }
}

export function getAnalytics(id){
  return function(dispatch) {
    const url =`${api}/product/${id}/sales`;
    const token = localStorage.getItem('token');

    axios({
      method: 'get',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
    .then(response => {
      dispatch({
        type: GET_ANALYTICS,
        volume: response.data.volume,
        total: response.data.total,
        buyers: response.data.buyers
      })
    })
    .catch(error => {

    })
  }
}