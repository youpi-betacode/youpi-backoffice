import { SET_LOADING } from '../types';

export function toggleLoading(toggle) {
  return function(dispatch) {
    dispatch({
      type: SET_LOADING,
      loading: toggle
    })
  }
}