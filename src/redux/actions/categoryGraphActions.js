import * as types from '../types';

export function setFilter(filter) {
  return(
    {type: types.SET_CATEGORY_FILTER,
    filter})
  
}