import { GET_VARIANT, SET_VARIANT_LABEL, SET_VARIANT_PRICE, SET_VARIANT_STOCK, SET_STOCK_CATEGORY, TOGGLE_SUCCESS_ADD, SET_SUCCESS_VARIANT_ADD, SET_UNSUCCESS_VARIANT_ADD, SET_SUCCESS_VARIANT_DELETE, SET_UNSUCCESS_VARIANT_DELETE, SET_SUCCESS_VARIANT_EDIT, SET_UNSUCCESS_VARIANT_EDIT } from '../types';
import axios from 'axios';
import { youpiAPI as api } from '../../constants';
import { push } from 'connected-react-router';

export function getVariant(id) {
  return function (dispatch) {
    const url = `${api}/variant/${id}`;
    const token = localStorage.getItem('token');
    
    axios({
      method: 'get',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
    .then(response => {
      dispatch({
        type: GET_VARIANT,
        id: response.data.id,
        reference: response.data.reference,
        label: response.data.label,
        price: response.data.price_base,
        stock: response.data.stock,
        productId: response.data.product
      })
      console.log(response)
    })
    .catch(error => {
    })
  }
}

export function setLabel(label) {
  return function (dispatch) {
    dispatch({
      type: SET_VARIANT_LABEL,
      label
    })
  }
}

export function setPrice(price) {
  return function(dispatch) {
    dispatch({
      type: SET_VARIANT_PRICE,
      price
    })
  }
}

export function setStock(stock) {
  return function(dispatch) {
    dispatch({
      type: SET_VARIANT_STOCK,
      stock
    })
  }
}

export function deleteVariant(id){
  return function(dispatch) {
    const url = `${api}/variant/${id}`;
    const token = localStorage.getItem('token');

    axios({
      method: 'delete',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
    .then(response => {
      dispatch(push(`/product/${response.data.product}`))
      dispatch(toggleSuccessDeleteVariant(true));
    })
    .catch(error => {
      dispatch(toggleUnsuccessDeleteVariant(true));
    })
  }
}

export function saveVariantEdit(data,id) {
  return function(dispatch){
    const url = `${api}/variant/${id}`;
    const token = localStorage.getItem('token');

    axios({
      method: 'put',
      url,
      data,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    .then(response => {
      dispatch(toggleSuccessEditVariant(true));
    })
    .catch(error => {
      dispatch(toggleUnsuccessEditVariant(true));
    })
  }
}

export function addNewVariant(data,productId) {
  return function(dispatch) {
    const url = `${api}/product/${productId}/variant`;
    const token = localStorage.getItem('token');

    axios({
      method: 'post',
      url,
      data,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    .then(response => {
      dispatch(push(`/variant/${response.data.id}`));
      dispatch(toggleSuccessAddVariant(true));
    })
    .catch(error => {
      dispatch(toggleUnsuccessAddVariant(true));
    })

  }
}

export function toggleSuccessAddVariant(toggle) {
  return {
    type: SET_SUCCESS_VARIANT_ADD,
    successAdd: toggle
  }
}

export function toggleUnsuccessAddVariant(toggle) {
  return {
    type: SET_UNSUCCESS_VARIANT_ADD,
    unsuccessAdd: toggle
  }
}

export function toggleSuccessDeleteVariant(toggle) {
  return {
    type: SET_SUCCESS_VARIANT_DELETE,
    successDelete: toggle
  }
}

export function toggleUnsuccessDeleteVariant(toggle) {
  return {
    type: SET_UNSUCCESS_VARIANT_DELETE,
    unsuccessDelete: toggle
  }
}

export function toggleSuccessEditVariant(toggle) {
  return {
    type: SET_SUCCESS_VARIANT_EDIT,
    successEdit: toggle
  }
}

export function toggleUnsuccessEditVariant(toggle) {
  return {
    type: SET_UNSUCCESS_VARIANT_EDIT,
    unsuccessEdit: toggle
  }
}