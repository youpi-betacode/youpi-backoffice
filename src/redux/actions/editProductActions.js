import { EDIT_PRODUCT_PAGE, TOGGLE_SUCCESS_DELETION, TOGGLE_SUCCESS_EDIT, TOGGLE_UNSUCCESS_DELETION, TOGGLE_UNSUCCESS_EDIT, TOGGLE_SUCCESS_ADD, TOGGLE_UNSUCCESS_ADD} from '../types'
import axios from 'axios';
import { push } from 'connected-react-router';
import { youpiAPI as api} from '../../constants';

export function toggleEditProduct() {
  return {
    type: EDIT_PRODUCT_PAGE
  }
}

export function toggleSuccessDeletion(toggle) {
  return {
    type: TOGGLE_SUCCESS_DELETION,
    successDeletion: toggle
  }
}

export function toggleSuccessEdit(toggle) {
  return {
    type: TOGGLE_SUCCESS_EDIT,
    successEdit: toggle
  }
}

export function toggleUnsuccessDeletion(toggle) {
  return {
    type: TOGGLE_UNSUCCESS_DELETION,
    unsuccessDeletion: toggle
  }
}

export function toggleUnsuccessEdit(toggle) {
  return {
    type: TOGGLE_UNSUCCESS_EDIT,
    unsuccessEdit: toggle
  }
}

export function saveNewProduct(formData,store) {
  
  const url = `${api}/store/${store}/product`;
  const token = localStorage.getItem('token');

  return function(dispatch) {
    axios({
      method: 'post',
      url,
      data: formData,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    .then(response => {
      dispatch(toggleSuccessAdd(true));
      dispatch(push('/stock'));
    })
    .catch(error => {
      dispatch(toggleUnsuccessAdd(true))
    })
  }
}

export function toggleSuccessAdd(toggle) {
  return {
    type: TOGGLE_SUCCESS_ADD,
    successAdd: toggle
  }
}

export function toggleUnsuccessAdd(toggle) {
  return {
    type: TOGGLE_UNSUCCESS_ADD,
    unsuccessAdd: toggle
  }
}