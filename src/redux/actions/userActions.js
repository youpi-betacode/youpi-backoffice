import { SET_USER } from '../types';
import axios from 'axios';
import { youpiAPI as api } from '../../constants';

export function getUser() {

  const token = localStorage.getItem("token")

  return function (dispatch) {

    axios({
      method: 'get',
      url: api + '/user',
      headers: { 'Authorization': 'Bearer ' + token },
    })
      .then((response) => {
        dispatch({
          type: SET_USER,
          firstName: response.data.first_name,
          lastName: response.data.last_name,
          email: response.data.email,
          store: response.data.store
        })
      })
      .catch((error) => {
      })
  }
}