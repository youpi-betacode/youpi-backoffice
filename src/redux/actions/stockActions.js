import * as types from '../types';
import axios from 'axios';
import { youpiAPI as api } from '../../constants';
import { toggleLoading } from './loadingActions'
import { downloadFile } from './commonUtils';

export function getProducts(page, pageSize, orderBy, desc, category, supplierID) {

  return function (dispatch) {
    const token = localStorage.getItem('token');

    dispatch(toggleLoading(true))

    axios({
      method: 'get',
      url: api + `/store/1/products?page=${page}&page_size=${pageSize}&order_by=${orderBy}&desc=${desc}&category=${category}&supplier=${supplierID}`,
      headers: { 
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json' 
      }
      
    })
      .then((response) => {
        dispatch({
          type: types.SET_PRODUCTS,
          products: response.data.products,
          count: response.data.count,
        })
        dispatch(toggleLoading(false))
      })
      .catch((error) => {
      })
  }
}

export function setPage(page){
  return function (dispatch){
    dispatch({
      type: types.SET_STOCK_PAGE,
      page: page
    })
  }
}

export function setPageSize(pageSize){
  return function (dispatch){
    dispatch({
      type: types.SET_STOCK_PAGE_SIZE,
      pageSize: pageSize
    })
  }
}

export function setOrder(order){
  return function (dispatch){
    dispatch({
      type: types.SET_STOCK_ORDER_BY,
      orderBy: order
    })
  }
}

export function toggleDesc(){
  return function (dispatch){
    dispatch({
      type: types.TOGGLE_STOCK_DESC,
    })
  }
}

export function setDesc(){
  return function (dispatch){
    dispatch({
      type: types.SET_STOCK_DESC,
    })
  }
}


export function setCategory(category){
  return function (dispatch){
    dispatch({
      type: types.SET_STOCK_CATEGORY,
      category: category
    })
  }
}

export function setSupplier(supplier){
  return function (dispatch){
    dispatch({
      type: types.SET_STOCK_SUPPLIER,
      supplier: supplier
    })
  }
}

export function getProduct(id){
  return function (dispatch){
    const token = localStorage.getItem('token');
    axios({
      method: 'get',
      url: api + `/product/${id}`,
      headers: { 
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json' 
      }
      
    })
    .then((response) => {
      dispatch({
        type: types.GET_PRODUCT,
        category: response.data.category,
        description: response.data.description,
        id: response.data.id,
        name: response.data.name
      })
    })
    .catch((error) => {
    })
  }
}

export function getStockFile(store) {
  return function() {
    const url = `${api}/store/${store}/products/export`;
    const token = localStorage.getItem('token');

    axios({
      method: 'get',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
    .then(response => {
      downloadFile(response.data, {type: 'text/csv', filename: 'Stock_report.csv'})
    })
    .catch(error => {
    })
  }
}
