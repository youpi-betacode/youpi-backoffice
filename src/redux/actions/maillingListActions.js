import * as types from '../types'
import axios from 'axios';
import { youpiAPI as api, page, pageSize } from '../../constants';
import { toggleLoading } from './loadingActions';
import { downloadFile } from './commonUtils';

export function toggleImportContactsModal() {
  return function(dispatch) {
    dispatch({
      type: types.TOGGLE_MODAL_IMPORT_CONTACTS
    })
  }
}

export function toggleSuccessfullUploadFile(success) {
  return function(dispatch) {
    dispatch({
      type: types.CHECK_SUCCESS_UPLOAD,
      successUpload: success
    })
  }
}

export function toggleInvalidFileSnackbar(open) {
  return function(dispatch) {
    dispatch({
      type: types.CHECK_VALID_FILE,
      validFile: open
    })
  }
}

export function importContacts(formData, orderBy='id', desc=true) {
  return function(dispatch) {

    const url = `${api}/contacts/import`;

    const token = localStorage.getItem('token');

    axios({
      method:'post',
      url: url,
      data: formData,
      headers: {'Authorization': `Bearer ${token}`}
    })
    .then((response) => {
      dispatch(
        loadContacts(page,pageSize,orderBy,desc),
        dispatch(toggleSuccessfullUploadFile(true))
      );
    })
    .catch((error) => {
      dispatch(
        setFileError(error.response.data.message),
        dispatch(toggleInvalidFileSnackbar(false))
      );
   })
  }
}

export function loadContacts(page,pageSize,orderBy,desc) {
  return function(dispatch){
    const url = `${api}/contacts?page=${page}&page_size=${pageSize}&order_by=${orderBy}&desc=${desc}`;
    const token = localStorage.getItem('token');

    dispatch(toggleLoading(true))

    axios({
      method:'get',
      url: url,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      dispatch({
        type: types.LOAD_CONTACTS,
        contacts: response.data.contacts,
        count: response.data.count,
        page: page,
        pageSize: pageSize
      })
      dispatch(toggleLoading(false))
    })
    .catch((error) => {
    })
  }
}

export function getContactsFile() {
  return function() {
    const url = `${api}/contacts/export`;
    const token = localStorage.getItem('token');

    axios({
      method: 'get',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
    .then(response => {
      downloadFile(response.data, {type: 'text/csv', filename: 'Mailling_list_eport.csv'})
    })
    .catch(error => {
    })
  }
}

export function setFileError(error){
  return function(dispatch) {
    dispatch({
      type: types.SET_FILE_ERROR,
      error: error
    })
  }
}

export function setPage(page) {
  return function(dispatch) {
    dispatch({
      type: types.SET_CONTACTS_PAGE,
      page: page
    })
  }
}

export function setPageSize(pageSize) {
  return function(dispatch) {
    dispatch({
      type: types.SET_CONTACTS_PAGE_SIZE,
      pageSize: pageSize
    })
  }
}

export function setOrder(order){
  return function (dispatch){
    dispatch({
      type: types.SET_CONTACTS_ORDER_BY,
      orderBy: order
    })
  }
}

export function toggleDesc(){
  return function (dispatch){
    dispatch({
      type: types.TOGGLE_CONTACTS_DESC,
    })
  }
}

export function setDesc(){
  return function (dispatch){
    dispatch({
      type: types.SET_CONTACTS_DESC,
    })
  }
}