export function downloadFile(data, options){
  var blob = new Blob([data], options);
  var url= window.URL.createObjectURL(blob);
  //window.open(url);

  var a = document.createElement("a");
  document.body.appendChild(a);
  a.setAttribute("style","display: none");
  a.href = url;
  a.download = options["filename"];
  a.click();
  window.URL.revokeObjectURL(url);
}