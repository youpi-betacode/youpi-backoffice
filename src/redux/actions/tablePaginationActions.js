import {ROWS_PER_PAGE,CURRENT_TABLE_PAGE} from '../types'

export function rowsPerPage(toShow) {
    return {
        type: ROWS_PER_PAGE,
        nrRows: toShow
    }
}

export const currentPage = (newPage) => ({
    type: CURRENT_TABLE_PAGE,
    currentPage: newPage
})