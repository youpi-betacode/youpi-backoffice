import { TOGGLE_DRAWER } from '../types';


export function toggleDrawer(){
    return function (dispatch) {
        dispatch({
            type: TOGGLE_DRAWER
        })
    }
}