import { LOGIN_EMAIL, LOGIN_PASSWORD, LOGOUT, SET_USER } from '../types';
import axios from 'axios';
import { youpiAPI as api } from '../../constants';
import { replace } from 'connected-react-router';

export const setEmail = (email) => ({
  type: LOGIN_EMAIL,
  email
})

export const setPassword = (password) => ({
  type: LOGIN_PASSWORD,
  password
})

export function login(email, password) {

  return function (dispatch) {
    const headers = new Headers()
    const body = {
      identifier: email,
      password: password
    }
    headers.append('Content-Type', 'application/json')
    const url = `${api}/login`
    axios({
      method: 'post',
      url: url,
      headers: headers,
      data: body
    })
      .then((response) => {
        localStorage.setItem('token', response.data.token);

        dispatch({
          type: SET_USER,
          firstName: response.data.first_name,
          lastName: response.data.last_name,
          email: response.data.email,
          store: response.data.store
        })

        dispatch(replace("/"))
      })
      .catch((error) => {
      })
  }
}


export function logout(token) {

  return function (dispatch) {

    axios({
      method: 'post',
      url: api + '/logout',
      headers: { 'Authorization': 'Bearer ' + token }
    })
      .then((response) => {

        dispatch({
          type: LOGOUT,
        })

        localStorage.removeItem('token')

        dispatch({
          type: SET_USER,
          firstName: '',
          lastName: '',
          email: ''
        })

        dispatch(replace("/login"))
      })
      .catch((error) => {
      })
  }
}
