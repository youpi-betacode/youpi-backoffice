import axios from 'axios';

import { GET_YOUPERS, SET_YOUPERS_PAGE, SET_YOUPERS_PAGE_SIZE, SET_YOUPERS_ORDER_BY, TOGGLE_YOUPERS_DESC, SET_YOUPERS_DESC } from '../types';
import { toggleLoading } from './loadingActions';
import { youpiAPI as api} from '../../constants';

export function getYoupers(page,pageSize,orderBy,desc,store=1) {
  return function(dispatch) {
    const url = `${api}/store/${store}/costumers?page=${page}&page_size=${pageSize}&order_by=${orderBy}&desc=${desc}`;
    const token = localStorage.getItem('token');

    dispatch(toggleLoading(true))

    axios({
      method: 'get',
      url: url,
      headers: {
        'Authorization' : `Bearer ${token}`
      }
    })
    .then(response => {
      dispatch({
        type: GET_YOUPERS,
        costumers: response.data.costumers,
        count: response.data.count
      })
      dispatch(toggleLoading(false));
    })
    .catch(error => {
      console.log(error.response);
    })
  }
}

export function setPage(page) {
  return function(dispatch) {
    dispatch({
      type: SET_YOUPERS_PAGE,
      page: page
    })
  }
}

export function setPageSize(pageSize) {
  return function(dispatch) {
    dispatch({
      type: SET_YOUPERS_PAGE_SIZE,
      pageSize: pageSize
    })
  }
}

export function setOrder(order){
  return function (dispatch){
    dispatch({
      type: SET_YOUPERS_ORDER_BY,
      orderBy: order
    })
  }
}

export function toggleDesc(){
  return function (dispatch){
    dispatch({
      type: TOGGLE_YOUPERS_DESC,
    })
  }
}

export function setDesc(){
  return function (dispatch){
    dispatch({
      type: SET_YOUPERS_DESC,
    })
  }
}

