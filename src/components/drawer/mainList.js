import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import { colors, iconsSrc } from '../../constants';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import Icon from '@material-ui/core/Icon'

function createData(key, label, icon, path) {
  return {key, label, icon, path};
}

const links = [
  createData(1, 'VISÃO GERAL', 'home', '/'),
  createData(2, 'GESTÃO DE STOCK', 'shopping_basket', '/stock'),
  createData(3, 'VENDAS', 'shopping_cart', '/sales'),
  createData(4, 'YOUPERS', 'people', '/youpers'),
  createData(5, 'PARCEIROS', 'group_work', '/partners'),
  createData(6, 'SPONSORS', 'attach_money', '/sponsors'),
  createData(7, 'MAILLING LIST', 'mail', '/maillingList'),
  createData(8, 'PROMOÇÕES', 'card_giftcard', '/promotions'),
  createData(9, 'DADOS ESTATÍSTICOS', 'bar_chart', '/statistics'),
  createData(10, 'CHAT', 'chat', '/chat'),
]

function MainList({push,pathname}) {
  const classes = useStyles();
  return (
    <div>
      {links.map(link =>
        <ListItem key={link.key} button onClick={() => push(link.path)} className={pathname===link.path ? classes.selectedItem : classes.unselectedItem}>
          <Icon className={pathname===link.path ? classes.selectedIcon : classes.unselectedIcon}>{link.icon}</Icon>
          <ListItemText primary={link.label} className={classes.itemText} />
        </ListItem>
      )}
    </div>
  )
};

const mapStateToProps = (state) => ({
  pathname: state.router.location.pathname
});

const mapDispatchToProps = {
  push
}

export default connect(mapStateToProps, mapDispatchToProps)(MainList)

const useStyles = makeStyles(theme => ({
  listSubHeader: {
    color: colors.primary
  },
  secondaryListIcon: {
    color: colors.secondaryListItems
  },
  iconPng: {
    fontSize: '24px',
    position: 'absolute'
  },
  itemText: {
    paddingLeft: '15px'
  },
  selectedItem: {
    '&:hover':{
      backgroundColor: colors.white,
      color: colors.primary
    },
    backgroundColor: colors.white,
    color: colors.primary
  },
  unselectedItem: {

  },
  selectedIcon: {
    color: colors.black
  },
  unselectedIcon:{
    color: colors.iconGreenish
  }
}))
