import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import { SecondaryListItems } from '../../pages/dashboard/listItems';
import { colors, drawerWidth } from '../../constants';
import Drawer from '@material-ui/core/Drawer';
import clsx from 'clsx';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import { connect } from 'react-redux';
import { toggleDrawer } from '../../redux/actions/drawerActions';
import MainList from './mainList';
import { push } from 'connected-react-router';

function AppDrawer(props) {
  const classes = useStyles();

  const {
    open,
    toggleDrawer,
  } = props;

  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
      }}
      open={open}
    >
      <div className={classes.toolbarIcon}>
        <IconButton onClick={toggleDrawer}>
          <ChevronLeftIcon />
        </IconButton>
      </div>
      <Divider/>
      <List>
        <MainList/>
      </List>
      <Divider />
      <List>{SecondaryListItems()}</List>
    </Drawer>
  );
}

const mapStateToProps = (state) => ({
  open: state.drawer.open,
});

const mapDispatchToProps = {
  toggleDrawer,
  push
}

export default connect(mapStateToProps, mapDispatchToProps)(AppDrawer)

const useStyles = makeStyles(theme => ({
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor: colors.transparent06
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(7),
    },
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    backgroundColor: colors.transparent06,
    ...theme.mixins.toolbar,
  },
}));