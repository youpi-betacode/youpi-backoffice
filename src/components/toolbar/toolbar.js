import React, { useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Button from '@material-ui/core/Button';
import { colors, iconsSrc, drawerWidth } from '../../constants'
import { Typography, Menu, MenuItem, Hidden, useMediaQuery } from '@material-ui/core';
import KeyboardArrowDown from "@material-ui/icons/KeyboardArrowDown";
import { useDispatch, useSelector } from 'react-redux';
import { toggleDrawer } from '../../redux/actions/drawerActions';
import { logout } from '../../redux/actions/loginActions';
import { getUser } from '../../redux/actions/userActions';
import Grid from '@material-ui/core/Grid';
import { push } from 'connected-react-router';

export default function AppToolbar({ width }) {
  const classes = useStyles();

  const open = useSelector(state => state.drawer.open);
  const firstName = useSelector(state => state.user.firstName);
  const lastName = useSelector(state => state.user.lastName);
  const token = localStorage.getItem('token');
  const dispatch = useDispatch();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [showIcon, setShowIcon] = React.useState(true)

  const handleClick = (event) => (
    setAnchorEl(event.currentTarget)
  )

  const handleClose = () => (
    setAnchorEl(null)
  )

  const handleLogout = () => {
    dispatch(logout(token))
  }

  useEffect(() => {
    dispatch(getUser())
  }, [])

  useEffect(() => {
    setTimeout(() => setShowIcon(!showIcon), 1000)
  }, [open])

  return (
    <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
      <Toolbar className={classes.toolbar}>
        <Grid container direction='row' justify='space-between' alignItems='center'>

          <Grid item xs={open ? 4 : 0} md={open ? 0 : 4}>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="Open drawer"
              onClick={() => dispatch(toggleDrawer())}
              className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
            >
              <MenuIcon />
            </IconButton>
          </Grid>

          <Hidden mdDown>
            <Grid item xs={6} lg={4} onMouseDown={4}>
              <Grid container justify={'center'}>
                <Button onClick={() => dispatch(push(''))}>
                  <img src={iconsSrc.logo_youpi} alt='YOUPI' width='122' />
                </Button>
              </Grid>

            </Grid>
          </Hidden>
          <Grid item xs={open ? 8 : 8} lg={4}>
            <Grid container direction='row' alignItems='center' justify='flex-end' spacing={2}>
              {/* <Grid item>
                <IconButton color="inherit" className={classes.notifications}>
                  <Badge badgeContent={0} color="error">
                    <NotificationsIcon />
                  </Badge>
                </IconButton>
              </Grid> */}

              <Grid item>
                <Button style={{ paddingLeft: 20, paddingRight: 20 }} onClick={handleClick}>
                  <Grid container direction='row' alignItems='center' spacing={2}>
                    <Hidden smDown>
                    <Grid item>
                      <Typography>
                        {firstName} {lastName}
                      </Typography>
                    </Grid>
                    </Hidden>
                    
                    <Grid item >
                      <img
                        src='/images/user_icon.png'
                        className={classes.avatar}
                        alt='User Avatar'
                      />
                    </Grid>
                    <Grid item>
                      <div style={{ paddingTop: 5, overflow: 'hidden' }}>
                        <KeyboardArrowDown />
                      </div>

                    </Grid>
                  </Grid>
                </Button>
                <Menu
                  id="simple-menu"
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                  anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
                  transformOrigin={{ vertical: -70, horizontal: 'right' }}
                >
                  <MenuItem onClick={handleLogout}>Logout</MenuItem>
                </Menu>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
}


const useStyles = makeStyles(theme => ({
  toolbar: {
    overflow: 'hidden'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  avatar: {
    width: '40px',
    height: '40px',
    borderRadius: '50%',
    float: 'right',
    marginTop: 5
  },
}));