const colors = {
    primary: "#b47b48",
    notification: "#fe5454",
    transparent: "rgba(0,0,0,0)",
    transparent01: "rgba(0,0,0,0.1)",
    transparent02: "rgba(0,0,0,0.2)",
    transparent03: "rgba(0,0,0,0.3)",
    transparent04: "rgba(0,0,0,0.4)",
    transparent06: "rgba(0,0,0,0.6)",
    transparentWhite04: "rgba(255,255,255, 0.4)",
    graphLight: "rgba(255,255,255,0.8)",
    iconGreenish: "#4fe3c1",
    secondaryListItems: "rgba(255,255,255,0.6)",
    paperBackgroundColor: "rgba(0,0,0,0.2)",
    orderStatusColor: "rgba(110, 200, 40, 1)",
    loginButton: '#22d3c5',
    loginButtonHover: 'rgba(34,211,197,0.8)',
    black: '#000000',
    white: '#ffffff',
    loginGrey: '#a8a8a8',
    lightGrey: '#bfbfbf',
    red: '#ff0000',
    success: '#43a047',
    errorRed: '#d32f2f'
    
}

const iconsSrc = {
    logo_youpi: '/images/logo_youpi.png',
    youpi_login: '/images/logo_login.png',
    dummyPillow: '/images/pillow.png',
    dummyPillow2: '/images/pillow2.png',
    left_lat: '/images/lat_esq.png',
    right_lat: '/images/lat_dir.png'
}

const drawerWidth = 300;

const page = 1;
const pageSize = 10;

const rowsPerPageOptions = [2,10, 25, 50];

//const youpiAPI = 'http://127.0.0.1:5000/v1';
const youpiAPI = 'http://api.youpii.betacode.tech/v1';//process.env.ENDPOINT;

export {colors, iconsSrc, drawerWidth, youpiAPI, page, pageSize, rowsPerPageOptions}