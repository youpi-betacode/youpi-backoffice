import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from '../pages/dashboard/components/home/home';
import OrdersList from '../pages/dashboard/components/sales/ordersList';
import ProductsList from '../pages/dashboard/components/stock/productsList';
import YoupersList from '../pages/dashboard/components/youpers/youpersList';
import PartnerList from '../pages/dashboard/components/partners/partnerList';
import SponsorList from '../pages/dashboard/components/sponsors/sponsorList';
import MaillingList from '../pages/dashboard/components/mail/maillingList';
import Promotions from '../pages/dashboard/components/promotions/promotions';
import Statistics from '../pages/dashboard/components/statistics/statistics';
import Chat from '../pages/dashboard/components/chat/chat';
import NotFound from '../pages/dashboard/components/notFound';
// import ProductDetails from '../pages/dashboard/components/stock/productDetails/productDetails'
import ProductDetails2 from '../pages/dashboard/components/stock/productDetails/productDetails2/productDetails2'
import NewProduct from '../pages/dashboard/components/stock/productDetails/newProduct';
import EditVariant from '../pages/dashboard/components/stock/productDetails/variants/editVariant';
import NewVariant from '../pages/dashboard/components/stock/productDetails/variants/newVariant';

const routes = (
    <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/stock' component={ProductsList}/>
        <Route path='/sales' component={OrdersList} />
        <Route path='/youpers' component={YoupersList}/>
        <Route path='/partners' component={PartnerList}/>
        <Route path='/sponsors' component={SponsorList}/>
        <Route path='/maillingList' component={MaillingList}/>
        <Route path='/promotions' component={Promotions}/>
        <Route path='/statistics' component={Statistics}/>
        <Route path='/chat' component={Chat}/>
        <Route path='/product/newproduct'  component={NewProduct}/>
        <Route path='/product/:id/newvariant' component={NewVariant}/>
        <Route path='/product/:id'  component={ProductDetails2}/>
        {/* <Route path='/product2/:id' component={ProductDetails2}/> */}
        <Route path ='/variant/:id' component={EditVariant}/>
        <Route component={NotFound} />
    </Switch>
)

export default routes;