import React from 'react';
import { ConnectedRouter } from 'connected-react-router';

import routes from './appRoutes';

const AppRouter = ({history}) => (
    <ConnectedRouter history={history}>
        { routes }
    </ConnectedRouter>
)

export default AppRouter;