import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Main from '../pages/main';
import Login from '../pages/login/loginPage';

const routes = (
    <Switch>
        <Route path='/login' component={Login} />
        <Route component={Main} />
    </Switch>
)

export default routes;