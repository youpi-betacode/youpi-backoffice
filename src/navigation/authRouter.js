import React from 'react';
import { ConnectedRouter } from 'connected-react-router';

import routes from './authRoutes';

const AuthRouter = ({history}) => (
    <ConnectedRouter history={history}>
        { routes }
    </ConnectedRouter>
)

export default AuthRouter;