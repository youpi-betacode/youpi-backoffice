import React from 'react';
import AuthRouter from '../navigation/authRouter';
import { Provider } from 'react-redux';
import configureStore, { history } from '../redux/configureStore';
import MuiThemeProvider from '@material-ui//core/styles/MuiThemeProvider';
import { createMuiTheme } from '@material-ui/core/styles';
import { colors } from '../constants';
import dotenv from 'dotenv';
import { SnackbarProvider } from 'notistack';

const result = dotenv.config();

const store = configureStore();

function app() {

  return (
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <SnackbarProvider maxSnack={3}>
          <AuthRouter history={history} />
        </SnackbarProvider>
      </Provider>
    </MuiThemeProvider>
  )
}

export default app;

const theme = createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      "@global": {
        body: {
          backgroundImage: "url('/images/background.jpg')",
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          overflow: 'hidden'
        },
        tableHeader: {
          backgroundColor: colors.transparent06,
          color: colors.white,
        }
      }
    },
    MuiTable: {
      "root": {
        width: '100%',
        backgroundColor: 'transparent'
      }
    },
    MuiTableHead: {
      "root": {
        backgroundColor: colors.transparent06,
      },
    },
    MuiTableRow: {
      root: {
        '&:nth-of-type(even)': {
          backgroundColor: colors.transparent04,
        },
        '&:nth-of-type(odd)': {
          backgroundColor: colors.transparent02,
        },
      },
      hover: {
        cursor: 'pointer'
      }
    },
    MuiTableCell: {
      "root": {
        textAlign: 'left',
        borderBottom: 0,
      },
      "head": {
        color: colors.white,
        fontSize: 12,
      },
      "body": {
        color: colors.white,
        fontSize: 10,
      },
    },
    MuiPaper: {
      root: {
        backgroundColor: colors.transparent,
        overflow: 'auto'
      },

    },
    MuiTablePagination: {
      root: {
        backgroundColor: colors.transparent04,
        width: "100%",
        display: "flex"
      }
    },
    MuiCheckbox: {
      colorPrimary: colors.white,
      colorSecondary: colors.white,
    },
    MuiMenu: {
      paper: {
        backgroundColor: colors.white,
        color: colors.black
      }
    },
    MuiTypography: {
      body1: {
        fontSize: '14px !important'
      }
    }
  },
  typography: {
    subtitle1: {
      textTransform: 'uppercase',
      fontSize: 12,
      fontWeight: 'bold'
    }
  },
  palette: {
    type: 'dark',
    primary: {
      // light: will be calculated from palette.primary.main,
      main: colors.primary,
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      light: '#0066ff',
      main: colors.white,
      // dark: will be calculated from palette.secondary.main,
      contrastText: '#ffcc00',
    }
    // error: will use the default color
  },
});