import React from 'react';
import { Redirect } from 'react-router-dom';
import Dashboard from './dashboard/dashboard';

export default function Main({history}) {

  if (localStorage.getItem('token')===null) {
    return <Redirect to="/login"/>;
  }
  return <Dashboard history={history}/>;
};
 