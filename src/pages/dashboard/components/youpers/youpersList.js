import React, { useEffect } from 'react'
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Searchbar from '../common/searchbar';
import Table from '../common/table';
import { useSelector, useDispatch } from 'react-redux';
import CustomButton from '../common/button';
import Loader from '../common/loading';
import { colors } from '../../../../constants';
import { getYoupers, toggleDesc, setDesc, setOrder, setPage, setPageSize } from '../../../../redux/actions/youpersActions';

function YoupersList() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const youpers = useSelector(state => state.youpers);

  const handleExportClick = () => {

  }

  const handleHeaderClick = (label) => {
    if(youpers.orderBy === label){
      dispatch(toggleDesc());
    }
    else{
      dispatch(setDesc());
      dispatch(setOrder(label));
    }
  }

  const handleChangePage = (event, newPage) => {
    dispatch(setPage(newPage + 1));
  }

  const handleChangeRowsPerPage = (event) => {
    dispatch(setPageSize(event.target.value))
  }

  useEffect(() => {
    dispatch(getYoupers(youpers.page,youpers.pageSize,youpers.orderBy,youpers.desc))
  },[youpers.page,youpers.pageSize,youpers.orderBy,youpers.desc])

  return (
    <div style={{ width: '100%' }}>
      <Paper>
        <Searchbar title='Youpers'>
          <CustomButton title='Exportar Report' action={handleExportClick} />
        </Searchbar>
        <Loader showChildren={true}>
          <Table
            table={youpers}
            items={youpers.costumers}
            sortBy={label => handleHeaderClick(label)}
            changePage={(event,newPage) => handleChangePage(event,newPage)}
            changeRowsPerPage={event => handleChangeRowsPerPage(event)}
          />
        </Loader>







        {/* <Searchbar title='Youpers'>
          <CustomButton title='Exportar Report' action={handleOnClick} />
        </Searchbar>
        <Loader showChildren={true}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  <TableSortLabel
                    children={'ID'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'NOME'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'EMAIL'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'LOCALIDADE'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'TELEFONE'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'VOLUME DE COMPRAS'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'TOTAL DE COMPRAS'}
                  />
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.slice(page * nrRows, page * nrRows + nrRows).map(row => (
                <TableRow key={row.id}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.email}</TableCell>
                  <TableCell>{row.location}</TableCell>
                  <TableCell>{row.phone}</TableCell>
                  <TableCell>{row.sales_volume}</TableCell>
                  <TableCell>{row.sales_total}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <TablePages count={rows.length} />
        </Loader> */}
      </Paper>
    </div>
  )
}

export default YoupersList;

const useStyles = makeStyles(theme => ({
  paper: {
    width: '100%'
  },
  categoryInput: {
    color: colors.white,
    fontSize: 12,
  },
}))