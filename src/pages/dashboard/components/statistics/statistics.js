import React from 'react';
import Paper from '@material-ui/core/Paper';
import { colors } from '../../../../constants';
import { makeStyles } from '@material-ui/core/styles';

function createData(id, name, email, location, products, contract_expiration_date, sales_physical_store, sales_online_store, sales_total) {
    return { id, name, email, location, products, contract_expiration_date, sales_physical_store, sales_online_store, sales_total };
}

const rows = [
    createData(0, 'Henhica', 'henhica@pimbolin.com', 'Henhicolandia', 25, "20/12/2019", 250, 300, 550),
    createData(0, 'Shportin', 'shportin@tranquilidade.pt', 'WC', 25, "20/12/2020", 250, 300, 550),
]

function Statistics() {
    const classes = useStyles();

    return (
        <div>
            Statistics
        </div>
    )
}

export default Statistics;

const useStyles = makeStyles(theme => ({
}));