import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dropzone from './dropFile';
import { colors } from '../../../../constants'

const ImportContacts = React.forwardRef((props,ref) => {

  const classes = useStyles();
  
  return (
    <div className={classes.modal}>
      {props.children}
    </div>
  )

})

export default ImportContacts;

const useStyles = makeStyles(theme => ({
  modal: {
    width: '40%',
    height: '50%',
    backgroundColor: colors.transparent06,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 4),
    outline: 'none',
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center'
  }
}));