import React from 'react';
import Dropzone from 'react-dropzone';
import { Icon } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles'
import { useDispatch } from 'react-redux';

import { importContacts, toggleInvalidFileSnackbar, toggleImportContactsModal, setFileError } from '../../../../redux/actions/maillingListActions';

function DropFile() {
  const styles = useStyles();
  const dispatch = useDispatch();

  const handleOnDrop = (files,rejectedFiles) => {

    if(rejectedFiles && rejectedFiles.length > 0){
      const currentRejectFile = rejectedFiles[0];
      const currentRejectFileType = currentRejectFile.type;
      if(currentRejectFileType !== 'text/csv')
        //alert('Apenas são permitidos ficheiros .csv');
        dispatch(setFileError('Apenas são permitidos ficheiros .csv'))
        dispatch(toggleInvalidFileSnackbar(false))
    }
    else{
      const formData = new FormData;
      formData.append('file', files[0]);
      dispatch(toggleImportContactsModal());
      dispatch(importContacts(formData));
    }
  }

  return (
    <React.Fragment>
      <Dropzone onDrop={handleOnDrop} acceptedFiles='text/csv'>
        {({getRootProps, getInputProps}) => (
          <section>
            <div {...getRootProps()} className={styles.drop}>
              <div className={styles.inputDiv}>
                <input {...getInputProps()} />
                <Icon style={{ fontSize: 60 }}>cloud_upload</Icon>
                <p>Selecione ou arraste o ficheiro aqui</p>
              </div>
            </div>
          </section>
        )}
      </Dropzone>
    </React.Fragment>
  )
}

export default DropFile;

const useStyles = makeStyles(theme => ({
  drop: {
    display: 'table',
    height: '400px',
    overflow: 'hidden',
    margin: 'auto',
    outlineStyle: 'none',
    cursor: 'pointer'
  },
  inputDiv: {
    display: 'table-cell',
    verticalAlign: 'middle'
  }
}))