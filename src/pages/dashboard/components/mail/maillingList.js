import React, { useEffect } from 'react'
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Searchbar from '../common/searchbar'
import CustomButton from '../common/button'
import { useDispatch, useSelector } from 'react-redux'
import { toggleImportContactsModal, loadContacts, setPage, setPageSize,toggleDesc, setDesc, setOrder, getContactsFile } from '../../../../redux/actions/maillingListActions'
import { Modal } from '@material-ui/core';
import ImportContacts from './importContacts';
import Loader from '../common/loading';
import Table from '../common/table';
import DropFile from './dropFile';

//todo: change onClick action when adding reduce
const handleOnClick = (e) => {
  e.preventDefault();
}


function MailingList() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const contacts = useSelector(state => state.maillingList);
  const ref = React.createRef();

  const handleImportClick = () => {
    dispatch(toggleImportContactsModal());
  }

  const handleModalClose = () => {
    dispatch(toggleImportContactsModal());
  }

  const handleHeaderClick = (label) => {
    if(contacts.orderBy === label){
      dispatch(toggleDesc());
    }
    else{
      dispatch(setDesc());
      dispatch(setOrder(label));
    }
  }

  const handleChangePage = (event, newPage) => {
    dispatch(setPage(newPage + 1));
  }

  const handleChangeRowsPerPage = (event) => {
    dispatch(setPageSize(event.target.value))
  }

  const handleExportClick = () => {
    dispatch(getContactsFile())
  }

  useEffect(() => {
    dispatch(loadContacts(contacts.page, contacts.pageSize, contacts.orderBy, contacts.desc))
  }, [contacts.page, contacts.pageSize, contacts.orderBy, contacts.desc])

  return (
    <div style={{ width: '100%' }}>
      <Paper>
        <Searchbar title='Mailling List' left={4} center={3} right={5}>
          <CustomButton title='Criar Newsletter' action={handleOnClick} />
          <CustomButton title='Importar Contactos' action={handleImportClick} />
          <CustomButton title='Exportar Report' action={handleExportClick} />
        </Searchbar>
        <Loader showChildren={true}>
          <Table
            table={contacts}
            items={contacts.contacts}
            sortBy={label => handleHeaderClick(label)}
            changePage={(event,newPage) => handleChangePage(event,newPage)}
            changeRowsPerPage={(event) => handleChangeRowsPerPage(event)}
          />
        </Loader>
      </Paper>
      <Modal open={contacts.modal} onClose={handleModalClose}>
        <ImportContacts ref={ref}>
          <DropFile/>
        </ImportContacts>
      </Modal>
    </div>
  )
}

export default MailingList;

const useStyles = makeStyles(theme => ({
}));