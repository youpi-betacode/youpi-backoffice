import React, { useEffect } from 'react';

import { Grid, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../../../../constants';
import Subtitle from '../subtitle';
import { PieChart, Pie, Cell, ResponsiveContainer, Tooltip, Legend, Label } from 'recharts';
import DateFilter from './dateFilter';

import { useSelector, useDispatch } from 'react-redux';
import { getCategoryGraph } from '../../../../../redux/actions/dashboardActions';


const COLORS = ['#eac183', '#d1e051', '#4ddac9', '#5083df', '#4ed8c8',
  '#fbe31f','#c21ef3','#7fd323','#4fe3c1','#ed112b','#427508','#0c69eb','#fb8e29',
];

const BarLabel =
  ({ cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent }) => {
    const RADIAN = Math.PI / 180;
    const radius = 25 + innerRadius + (outerRadius - innerRadius);
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill={colors.white}
        textAnchor={x > cx ? "start" : "end"}
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  }

const CenterLabel = ({ viewBox, totalSales }) => {
  const { cx, cy } = viewBox;
  return (

    <text x={cx} y={cy} fill={colors.white} width={30} textAnchor="middle" dominantBaseline="central">
      <tspan x={cx} fontSize="26">{totalSales}</tspan>
      <tspan x={cx} fontSize="14" dy="1.5em">Vendas</tspan>
    </text>
  )
}

function CategorySales(props) {

  const classes = useStyles();
  const dispatch = useDispatch();
  const categoryData = useSelector(state => state.dashboard.categoryData);
  const categoryTotal = useSelector(state => state.dashboard.categoryTotal)
  const filter = useSelector(state => state.categoryGraph.filter);



  useEffect(() => {
    dispatch(getCategoryGraph(filter))
  }, [filter])

  return (
    <Grid item md={6} xs={12}>
      <Grid container spacing={0}>
        <Subtitle title={"Vendas por Categoria"} />
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Grid container>
              <Grid item md={3} xs={12}>
                <DateFilter />
              </Grid>
              <Grid item md={9} xs={12}>
                <div style={{ width: '100%', height: 400, overflow: 'hidden' }}>
                  {
                    categoryTotal === 0
                    &&
                    <Grid  container justify='center' alignItems='center' style={{height: '100%'}}>
                      <Grid item>
                        <Typography >
                          Sem vendas na última semana
                        </Typography>                        
                      </Grid>
                    </Grid>
                  }
                  {
                    categoryTotal !== 0
                    &&
                    <ResponsiveContainer>
                      <PieChart width={800} height={400}>
                        <Legend
                          verticalAlign="bottom"
                          align="right"
                          wrapperStyle={{
                            paddingTop: "10px"
                          }}
                          iconType='circle'
                        />

                        <Pie
                          dataKey="value"
                          data={categoryData}
                          innerRadius={90}
                          outerRadius={120}
                          labelLine={false}
                          label={BarLabel}
                          stroke='none'

                        >
                          <Label
                            position='center'
                            style={{ textAnchor: 'middle', fontSize: 16, fill: colors.white }}
                            content={<CenterLabel totalSales={categoryTotal} />}

                          />
                          {
                            categoryData.map((entry, index) =>
                              <Cell
                                key={`cell-${index}`}
                                fill={COLORS[index % COLORS.length]}
                              />
                            )
                          }
                        </Pie>
                        <Tooltip />
                      </PieChart>
                    </ResponsiveContainer>
                  }

                </div>

              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </Grid>

  )
}

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    color: colors.white,
    backgroundColor: colors.transparent01,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
  },

}));

export default CategorySales;