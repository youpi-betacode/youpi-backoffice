import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../../../../constants';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';
import { useSelector, useDispatch } from 'react-redux';
import { setFilter } from '../../../../../redux/actions/categoryGraphActions';


function DateFilter(props) {

  const classes = useStyles();
  const filter = useSelector(state => state.categoryGraph.filter);
  const dispatch = useDispatch();

  return (    
    <Grid 
      container
      direction="column"
      justify="center"
      className={classes.wrapper}
      spacing={2}
    >
        <Button 
          variant="outlined" 
          onClick={()=> dispatch(setFilter('WEEK'))}
          className={clsx(classes.button, filter==='WEEK' && classes.selectedButton)} 
        >
          Semana
        </Button>
        <Button 
          variant="outlined" 
          onClick={()=> dispatch(setFilter('MONTH'))}
          className={clsx(classes.button, filter==='MONTH' && classes.selectedButton)} 
        >
          Mês
        </Button>
        <Button 
          variant="outlined" 
          onClick={()=> dispatch(setFilter('YEAR'))}
          className={clsx(classes.button, filter==='YEAR' && classes.selectedButton)} 
        >
          Ano
        </Button>
    </Grid>           

  )
}

const useStyles = makeStyles(theme => ({
  wrapper: {
    height: '100%',
  },
  button: {
    marginBottom: '5px',
    marginLeft: '10px'
  },
  selectedButton : {
    backgroundColor: colors.white,
    color: colors.black,
    '&:hover': {
      backgroundColor: colors.white,
    }
  }
}));

export default DateFilter;