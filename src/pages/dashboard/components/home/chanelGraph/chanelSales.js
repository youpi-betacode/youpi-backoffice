import React, { useEffect } from 'react';

import { Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { colors } from '../../../../../constants';
import Subtitle from '../subtitle';
import { BarChart, Bar, Cell, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { getChannelGraph } from '../../../../../redux/actions/dashboardActions';

const CustomizedLabel = ({ x, y, fill, value, width, height }) => {

  return (
    <text
      x={x + width}
      y={y + 0.5*height}
      dx={5}
      fill={fill}
      alignmentBaseline="central"
      textAnchor="center">{value}€
    </text>
  )
};

function ChannelSales(props) {

  const classes = useStyles();


  const dispatch = useDispatch();
  const channelData = useSelector(state => state.dashboard.channelData);

  useEffect(() => {
    dispatch(getChannelGraph())
  }, [])


  return (
    <Grid item md={6} xs={12}>
      <Grid container spacing={0}>
        <Subtitle title={"Vendas por Canal"} />
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <div style={{ width: '100%', height: 400, overflow: 'hidden' }}>
              <ResponsiveContainer>
                <BarChart
                  layout={'vertical'}
                  data={channelData}
                  margin={{ top: 0, right: 60, left: 15, bottom: 0 }}

                  className={classes.chart}
                >
                  {/* <Tooltip cursor={{ fill: colors.transparent02, strokeWidth: 0 }}/> */}
                  <XAxis hide type='number' />
                  <YAxis
                    stroke={colors.white}
                    type='category'
                    dataKey="name"
                    tickSize={0}
                    tickMargin={10}
                    axisLine={false}
                  />
                  <Legend
                    verticalAlign="bottom"
                    align="right"
                    wrapperStyle={{
                      paddingTop: "10px"
                    }}
                    iconType='circle'
                  />
                  <Bar
                    dataKey="Volume"
                    fill={colors.graphLight}
                    label={{ position: 'right' }}
                    margin={{right: 40}}
                    radius={[3, 3, 3, 3]}
                  >
                    {
                      channelData.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={colors.graphLight} />
                      ))
                    }
                  </Bar>
                  <Bar
                    dataKey="Valor"
                    fill={colors.iconGreenish}
                    label={<CustomizedLabel position={"right"}/>}
                    radius={[3, 3, 3, 3]}
                  >
                    {
                      channelData.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={colors.iconGreenish} />
                      ))
                    }
                  </Bar>
                </BarChart>
              </ResponsiveContainer>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </Grid>

  )
}

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    color: colors.white,
    backgroundColor: colors.transparent01,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
  },
}));

export default ChannelSales;


