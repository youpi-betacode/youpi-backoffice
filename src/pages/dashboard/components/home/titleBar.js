import React from 'react';

import { Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../../../constants';

function TitleBar(props) {

  const classes = useStyles();

  return (
    <Grid item xs={12}>
      <Paper className={classes.paper}>Visão Geral</Paper>
    </Grid>

  )
}

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: colors.primary,
    backgroundColor: colors.white,
    fontSize: 16,
    fontWeight: 'bold'
  },
}));

export default TitleBar;