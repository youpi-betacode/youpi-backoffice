import React from 'react';

import { Grid, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../../../constants';

function Subtitle({title}) {

  const classes = useStyles();

  return (
    <Grid item xs={12}>
      <Paper className={classes.paper}>
        <Typography variant="subtitle1">
          {title}
        </Typography>        
      </Paper>
    </Grid>

  )
}

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    color: colors.white,
    backgroundColor: colors.transparent04,   
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    marginBottom: 5,
  },
}));

export default Subtitle;