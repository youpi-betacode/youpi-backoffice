import React, { useEffect } from 'react';

import { Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../../../../constants';
import Subtitle from '../subtitle';

import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
} from 'recharts';

import { useSelector, useDispatch } from 'react-redux';
import { getProductGraph } from '../../../../../redux/actions/dashboardActions';

const productTypes = [
  'YOUPi! Pack',
  'Best',
  'Feeling',
  'Velas',
  'Ambientadores',
  'Almofadas',
  'Papel de Parede',
  'Atoalhados',
  'Capas de Edredon'
]

const legendColors = [
  '#4ed8c8',
  '#fbe31f',
  '#c21ef3',
  '#7fd323',
  '#4fe3c1',
  '#ed112b',
  '#427508',
  '#0c69eb',
  '#fb8e29',
]

function ProductSales(props) {

  const classes = useStyles();

  const dispatch = useDispatch();
  const data = useSelector(state=>state.dashboard.productData)

  useEffect(()=>{
    dispatch(getProductGraph())
  }, [])

  return (
    <Grid item md={6} xs={12}>
      <Grid container spacing={0}>
        <Subtitle title={"Vendas por Produto"} />
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <div style={{ width: '100%', height: 400, overflow: 'hidden' }}>
              <ResponsiveContainer>
                <LineChart
                  data={data}
                  margin={{ top: 20, right: 50, left: 20, bottom: 5 }}
                >
                  <XAxis
                    dataKey="name"

                    stroke={colors.white}
                    tickSize={0}
                    tickMargin={20}
                  />
                  <YAxis
                    stroke={colors.white}
                    tickSize={0}
                    tickMargin={20} />
                  <Tooltip />
                  <Legend verticalAlign="bottom"
                    align="right"
                    wrapperStyle={{
                      paddingTop: "20px"
                    }}
                    iconType='circle'
                  />

                  {productTypes.map((type, index) =>
                    <Line
                      key={`product-${index}`}
                      dataKey={type}
                      stroke={legendColors[index]}
                      strokeWidth={3}
                      dot={false}
                    />
                  )}

                </LineChart>
              </ResponsiveContainer>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </Grid>

  )
}

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    color: colors.white,
    backgroundColor: colors.transparent01,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
  },
}));

export default ProductSales;