import React, { useEffect } from 'react';
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Grid, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../../../../constants';
import Subtitle from '../subtitle';
import { BarChart, Bar, Cell, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import Hidden from '@material-ui/core/Hidden';

import { useSelector, useDispatch } from 'react-redux';
import { getSponsorGraph } from '../../../../../redux/actions/dashboardActions';


// const sponsorData = [
//   {
//     name: "Sponsor 1",
//     data: [
//       {
//         name: 'A', Volume: 4000, Valor: 2400,
//       },
//       {
//         name: 'B', Volume: 9000, Valor: 1398,
//       },
//       {
//         name: 'C', Volume: 2500, Valor: 2450,
//       },
//       {
//         name: 'D', Volume: 3300, Valor: 2698,
//       },
//     ]
//   },
//   {
//     name: "Sponsor 2",
//     data: [
//       {
//         name: 'Formato A', Volume: 2349, Valor: 4568,
//       },
//       {
//         name: 'Formato B', Volume: 6458, Valor: 1234,
//       },
//       {
//         name: 'Formato C', Volume: 5545, Valor: 6542,
//       },
//       {
//         name: 'Formato D', Volume: 2222, Valor: 1789,
//       },
//     ]
//   }
// ]


const CustomizedLabel = ({ x, y, fill, value, width, height }) => {

  return (
    <text
      x={x + width}
      y={y + 0.5*height}
      dx={5}
      fill={fill}
      alignmentBaseline="central"
      textAnchor="center">{value}€
    </text>
  )
};


function SponsorSales(props) {

  const classes = useStyles();

  const dispatch = useDispatch();
  const sponsorData = useSelector(state=>state.dashboard.sponsorData)

  useEffect(()=>{
    dispatch(getSponsorGraph())
  }, [])

  return (
    <Grid item md={6} xs={12}>
      <Grid container spacing={0}>
        <Subtitle title={"Vendas por Sponsor"} />
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Slider
              dots={true}
              infinite={true}
              className={classes.carousel}
            >
              {
                sponsorData.map((sponsor, index) =>
                  <div key={`sponsor-${index}`}>
                    <Grid container direction="column" spacing={2}>
                      <Grid item xs={12}>
                        <Typography variant="h6" align="center">
                          {sponsor.name}
                        </Typography>
                      </Grid>


                      <Grid item>
                        <Grid container >
                          <Hidden mdUp>
                            <Grid item xs={12}>
                              <div style={{ width: '100%', height: 323.5 }}>
                                <ResponsiveContainer>
                                  <BarChart
                                    layout={'vertical'}
                                    data={sponsor.data}
                                    margin={{ top: 0, right: 20, left: 35, bottom: 0 }}
                                  >
                                    <XAxis hide type='number' />
                                    <YAxis
                                      stroke={colors.white}
                                      type='category'
                                      dataKey="name"
                                      tickSize={0}
                                      tickMargin={10}
                                      axisLine={false}
                                    />
                                    <Legend
                                      verticalAlign="bottom"
                                      align="right"
                                      wrapperStyle={{
                                        paddingTop: "10px"
                                      }}
                                      iconType='circle'
                                    />
                                    <Bar
                                      dataKey="Volume"
                                      fill={colors.graphLight}
                                      label={{ position: 'right' }}
                                      radius={[3, 3, 3, 3]}
                                    >
                                      {
                                        sponsor.data.map((entry, index) => (
                                          <Cell key={`cell-${index}`} fill={colors.graphLight} />
                                        ))
                                      }
                                    </Bar>
                                    <Bar
                                      dataKey="Valor"
                                      fill={colors.iconGreenish}
                                      label={<CustomizedLabel/>}
                                      radius={[3, 3, 3, 3]}
                                    >
                                      {
                                        sponsor.data.map((entry, index) => (
                                          <Cell key={`cell-${index}`} fill={colors.iconGreenish} />
                                        ))
                                      }
                                    </Bar>
                                  </BarChart>
                                </ResponsiveContainer>
                              </div>
                            </Grid>
                          </Hidden>

                          <Hidden smDown>
                            <Grid item xs={6}>
                              <div style={{ width: '100%', height: 323.5 }}>
                                <ResponsiveContainer>
                                  <BarChart
                                    layout={'vertical'}
                                    data={sponsor.data}
                                    margin={{ top: 0, right: 40, left: 35, bottom: 0 }}
                                  >
                                    <XAxis hide type='number' />
                                    <YAxis
                                      stroke={colors.white}
                                      type='category'
                                      dataKey="name"
                                      tickSize={0}
                                      tickMargin={10}
                                      axisLine={false}
                                    />
                                    <Legend
                                      verticalAlign="bottom"
                                      align="right"
                                      wrapperStyle={{
                                        paddingTop: "10px"
                                      }}
                                      iconType='circle'
                                    />
                                    <Bar
                                      dataKey="Volume"
                                      fill={colors.graphLight}
                                      label={{ position: 'right' }}
                                      radius={[3, 3, 3, 3]}
                                    >
                                      {
                                        sponsor.data.map((entry, index) => (
                                          <Cell key={`cell-${index}`} fill={colors.graphLight} />
                                        ))
                                      }
                                    </Bar>
                                  </BarChart>
                                </ResponsiveContainer>
                              </div>
                            </Grid>
                            <Grid item xs={6}>
                              <div style={{ width: '100%', height: 323.5 }}>
                                <ResponsiveContainer>
                                  <BarChart
                                    layout={'vertical'}
                                    data={sponsor.data}
                                    margin={{ top: 0, right: 60, left: 35, bottom: 0 }}
                                  >
                                    <XAxis hide type='number' />
                                    <YAxis
                                      stroke={colors.white}
                                      type='category'
                                      dataKey="name"
                                      tickSize={0}
                                      tickMargin={10}
                                      axisLine={false}
                                    />
                                    <Legend
                                      verticalAlign="bottom"
                                      align="right"
                                      wrapperStyle={{
                                        paddingTop: "10px"
                                      }}
                                      iconType='circle'
                                    />
                                    <Bar
                                      dataKey="Valor"
                                      fill={colors.iconGreenish}
                                      label={<CustomizedLabel/>}
                                      radius={[3, 3, 3, 3]}
                                    >
                                      {
                                        sponsor.data.map((entry, index) => (
                                          <Cell key={`cell-${index}`} fill={colors.iconGreenish} />
                                        ))
                                      }
                                    </Bar>
                                  </BarChart>
                                </ResponsiveContainer>
                              </div>
                            </Grid>

                          </Hidden>

                        </Grid>
                      </Grid>
                    </Grid>

                  </div>
                )
              }

            </Slider>
          </Paper>
        </Grid>
      </Grid>
    </Grid>

  )
}

const useStyles = makeStyles(theme => ({
  paper: {
    padding: 20,
    color: colors.white,
    backgroundColor: colors.transparent01,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
  },
  carousel: {
    margin: '20px',
    marginTop: 0,
  },
}));

export default SponsorSales;