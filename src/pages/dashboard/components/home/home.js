import React from 'react';

import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../../../constants';
import TitleBar from './titleBar';
import ChanelSales from './chanelGraph/chanelSales';
import CategorySales from './categoryGraph/categorySales';
import SponsorSales from './sponsorGraph/sponsorSales';
import ProductSales from './productGraph/productSales';

function Home(props) {

    const classes = useStyles();

    return (
        <Grid container spacing={2}>
            <TitleBar/>
            <ChanelSales/>
            <CategorySales/>
            <SponsorSales/>
            <ProductSales/>
        </Grid>
    )
}

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: colors.white,        
    },
}));

export default Home;