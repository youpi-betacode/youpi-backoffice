import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';
import Searchbar from '../common/searchbar';
import { useSelector, useDispatch } from 'react-redux';
import CustomButton from '../common/button';
import { colors } from '../../../../constants';
import Loader from '../common/loading';
import Table from '../common/table';
import { push } from 'connected-react-router';
import { setOrder, toggleDesc, setDesc, setPage, setPageSize, getProducts, getStockFile } from '../../../../redux/actions/stockActions';


function ProductsList() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const stock = useSelector(state => state.stock);
  const message = useSelector(state => state.product.deletionMessage);
  const user = useSelector(state => state.user);

  const handleRowClick = (id) => {
    dispatch(push(`/product/${id}`));
  }

  const handleExportClick = () => {
    dispatch(getStockFile(user.store))
  }

  const handleAddProductClick = () => {
    dispatch(push('/product/newproduct'))
  }
  

  const handleHeaderClick = (label) => {
    if(stock.orderBy === label){
      dispatch(toggleDesc());
    }
    else{
      dispatch(setDesc());
      dispatch(setOrder(label));
    }
  }

  const handleChangePage = (event, newPage) => {
    dispatch(setPage(newPage + 1));
  }

  const handleChangeRowsPerPage = (event) => {
    dispatch(setPageSize(event.target.value))
  }

  useEffect(() => {
    dispatch(getProducts(stock.page, stock.pageSize, stock.orderBy, stock.desc, stock.category, stock.supplier))
  }, [stock.page, stock.pageSize, stock.orderBy, stock.desc, stock.category, stock.supplier, message])

  return (

    <Paper className={classes.paper}>
      <Searchbar title='Gestão de Stock'>
        <CustomButton title='Adicionar Produto' action={handleAddProductClick}/>
        <CustomButton title='Exportar Report' action={handleExportClick}/>
      </Searchbar>
      <Loader showChildren={true}>
        <Table
          table={stock}
          items = {stock.products}
          action={id => handleRowClick(id)}
          sortBy={label => handleHeaderClick(label)}
          changePage={(event,newPage) => handleChangePage(event, newPage)}
          changeRowsPerPage={(event) => handleChangeRowsPerPage(event)}
        />
      </Loader>
    </Paper>
  )
}

export default ProductsList;


const useStyles = makeStyles(theme => ({
  paper: {
    width: '100%'
  },
  categoryInput: {
    color: colors.white,
    fontSize: 12,
  },
}));

