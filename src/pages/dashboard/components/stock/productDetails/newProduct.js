import React from 'react';
import { Grid, Card, Button, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useSelector, useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import clsx from 'clsx';

import GeneralInfo from './productDetails2/generalInfo';
import Variants from './productDetails2/variants';
import ProductProperties from './productDetails2/productProperties';
import Photos from './productDetails2/photos';
import Footer from '../../common/footer';
import { colors } from '../../../../../constants';
import { toggleDesc, setDesc, setOrder, setPage, setPageSize } from '../../../../../redux/actions/productActions';
import { saveNewProduct } from '../../../../../redux/actions/editProductActions';

function NewProduct() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const product = useSelector(state => state.product);
  const user = useSelector(state => state.user);

  const handleHeaderClick = (label) => {
    if (product.orderBy === label) {
      dispatch(toggleDesc());
    }
    else {
      dispatch(setDesc());
      dispatch(setOrder(label));
    }
  }

  const handleChangePage = (event, newPage) => {
    dispatch(setPage(newPage + 1));
  }

  const handleChangeRowsPerPage = (event) => {
    dispatch(setPageSize(event.target.value))
  }

  const handleOnCancelClick = () => {
    dispatch(push('/stock'));
  }

  const handleOnNewClick = () => {
    const formData = new FormData;
    formData.append('name', product.name);
    formData.append('description', product.description);
    formData.append('category', product.category);
    formData.append('supplier', 1);
    dispatch(saveNewProduct(formData, user.store));
  }

  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card className={classes.card}>
            <GeneralInfo />
          </Card>
        </Grid>
        <Grid item xs={12}>
          <Card className={classes.card}>
            <ProductProperties />
          </Card>
        </Grid>
        <Grid item xs={12}>
          <Card className={classes.card}>
            <Photos />
          </Card>
        </Grid>
        <Grid item xs={12}>
          <Card className={classes.card}>
            <Variants
              table={product}
              items={product.variants}
              sortBy={label => handleHeaderClick(label)}
              changePage={(event, newPage) => handleChangePage(event, newPage)}
              changeRowsPerPage={(event) => handleChangeRowsPerPage(event)}
            />
          </Card>
        </Grid>
      </Grid>
      <Card className={clsx(classes.card, classes.footerCard)}>
        <Footer>
          <CardContent style={{ transform: 'translateY(7%)' }}>
            <Grid container>
              <Grid item xs={6} style={{textAlign: 'left'}}>
                <Button variant='outlined' onClick={handleOnCancelClick}>cancelar</Button>
              </Grid>
              <Grid item xs={6} style={{textAlign: 'right'}}>
                <Button variant='outlined' onClick={handleOnNewClick}>Guardar</Button>
              </Grid>
            </Grid>
          </CardContent>
        </Footer>
      </Card>
    </div>
  )
}

export default NewProduct;

const useStyles = makeStyles(theme => ({
  card: {
    backgroundColor: colors.transparent02,
    paddingLeft: 10,
    paddingRight: 10
  },
  footerCard: {
    position: 'sticky',
    bottom: 0,
    left: 0,
    marginTop: 20,
    backgroundColor: colors.primary,
    zIndex: 1000
  }
}))