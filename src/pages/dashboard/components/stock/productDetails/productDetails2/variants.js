import React from 'react';
import { CardContent, Grid, Button } from '@material-ui/core';
import Table from '../../../common/table';
import Loader from '../../../common/loading';
import { useSelector } from 'react-redux';

function Variants(props) {
  const productId = useSelector(state => state.product.id);

  return (
    <CardContent>
      <Grid container direction='row' justify='space-between' alignItems='center'>
        <Grid item>
          <h2>VARIANTES DO PRODUTO</h2>
        </Grid>
        <Grid item>
          <Button variant='outlined' onClick={() => props.addVariant(productId)}><b>Adicionar variante</b></Button>
        </Grid>
      </Grid>
      <Loader showChildren={true}>
      <Table
        table={props.table}
        items={props.items}
        action={id => props.action(id)}
        sortBy={(label) => props.sortBy(label)}
        changePage={(event, newPage) => props.changePage(event, newPage)}
        changeRowsPerPage={(event) => props.changeRowsPerPage(event)}
      />
      </Loader>
    </CardContent>
  )
}

export default Variants;