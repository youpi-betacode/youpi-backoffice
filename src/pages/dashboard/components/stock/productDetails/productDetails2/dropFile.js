import React from 'react';
import Dropzone from 'react-dropzone';
import { Icon } from '@material-ui/core';

function DropFile({handleOnDrop, handleDropReject, accept='image/*'}) {

  const onDrop = (files, rejectedFiles, e) => {
    handleOnDrop(files, rejectedFiles, e);
  }

  const onDropReject = (files, e) => {
    handleDropReject(files,e);
  }

  return (
    <React.Fragment>
      <Dropzone onDrop={onDrop} onDropRejected={onDropReject} acceptedFiles={accept}>
        {({getRootProps, getInputProps}) => (
          <section>
            <div {...getRootProps()} className={styles.drop}>
              <div className={styles.inputDiv}>
                <input {...getInputProps()} />
                <Icon style={{ fontSize: 60 }}>cloud_upload</Icon>
                <p>Selecione ou arraste a imagem aqui</p>
              </div>
            </div>
          </section>
        )}
      </Dropzone>
    </React.Fragment>
  )
}

export default DropFile;