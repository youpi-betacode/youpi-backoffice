import React from 'react';
import { CardContent, Grid, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useSelector, useDispatch } from 'react-redux';
import { setCategory } from '../../../../../../redux/actions/productActions';
import { colors } from '../../../../../../constants';

function ProductProperties() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const product = useSelector(state => state.product);

  const handleCategoryChange = (event) => {
    dispatch(setCategory(event.target.value));
  }

  return (
    <React.Fragment>
      <CardContent>
        <h2>PROPRIEDADES DO PRODUTO</h2>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              label="Categoria"
              variant='outlined'
              // className={classes.textField}
              value={product.category}
              onChange={event => handleCategoryChange(event)}
              margin="normal"
              fullWidth
              // InputLabelProps={{
              //   classes: {
              //     root: classes.labelFocused
              //   }
              // }}
              InputProps={{
                classes: {
                  notchedOutline: classes.inputs
                }
              }}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              disabled
              label="Etiqueta de Produto (disabled)"
              variant='outlined'
              // className={classes.textField}
              value=''
              // onChange={handleChange('name')}
              margin="normal"
              fullWidth
              // InputLabelProps={{
              //   classes: {
              //     root: classes.labelFocused
              //   }
              // }}
              InputProps={{
                classes: {
                  notchedOutline: classes.inputs
                }
              }}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              disabled
              label="Peso (disabled)"
              variant='outlined'
              // className={classes.textField}
              value=''
              // onChange={handleChange('name')}
              margin="normal"
              fullWidth
              // InputLabelProps={{
              //   classes: {
              //     root: classes.labelFocused
              //   }
              // }}
              InputProps={{
                classes: {
                  notchedOutline: classes.inputs
                }
              }}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              disabled
              label="Custo de Transporte (disabled)"
              variant='outlined'
              // className={classes.textField}
              value=''
              // onChange={handleChange('name')}
              margin="normal"
              fullWidth
              // InputLabelProps={{
              //   classes: {
              //     root: classes.labelFocused
              //   }
              // }}
              InputProps={{
                classes: {
                  notchedOutline: classes.inputs
                }
              }}
            />
          </Grid>
        </Grid>
      </CardContent>
    </React.Fragment>
  )
}

export default ProductProperties;

const useStyles = makeStyles(theme => ({
  inputs: {
    borderRadius: 10,
    //borderColor: colors.white + '!important'
  },
  labelFocused: {
    color: colors.white + '!important'
  }
}))