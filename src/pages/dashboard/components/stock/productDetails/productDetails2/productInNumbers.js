import React , {useEffect} from 'react';
import { makeStyles } from '@material-ui/styles';
import { CardContent, Grid, Typography } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';

import { getAnalytics } from '../../../../../../redux/actions/productActions';
import { colors } from '../../../../../../constants';

function ProductInNumbers(props) {
  const classes = useStyles();
  const dispatch = useDispatch();

  const product = useSelector(state => state.product);

  useEffect(() => {
      dispatch(getAnalytics(props.productId))
  },[product.value, product.total, product.buyers])

  return (
    <React.Fragment>
      <CardContent>
          <h2>PRODUTO EM NÚMEROS</h2>
          <Grid container  direction="row" justify="space-evenly" alignItems="center">
            <Grid>
              <div className={classes.numberDivs}>
                {product.volume}
              </div>
              <div style={{textAlign: 'center'}}>
                Nº de Vendas
              </div>
            </Grid>
            <Grid>
              <div className={classes.numberDivs}>
                  {product.total}€
              </div>
              <div style={{textAlign: 'center'}}>
                Valor de Vendas
              </div>
            </Grid>
            <Grid>
              <div className={classes.numberDivs}>
                {product.buyers}
              </div>
              <div style={{textAlign: 'center'}}>
                Nº de Compradores
              </div>
            </Grid>
          </Grid>
      </CardContent>
    </React.Fragment>
  )
}

export default ProductInNumbers;

const useStyles = makeStyles(theme => ({
  numberDivs: {
    backgroundColor: colors.lightGrey,
    color: colors.black,
    border: '1px solid',
    borderColor: colors.lightGrey,
    width: 120,
    height: 120,
    borderRadius: '50%',
    textAlign: 'center',
    lineHeight: '120px',
    fontSize: 24,
    margin: '10px 10px',
    wordWrap: 'break-word'
  },
  circlesDiv: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row'
    
  }
}))