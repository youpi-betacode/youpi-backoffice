import React, { useState } from 'react';
import { CardContent, Grid, Button, Hidden, Modal } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { iconsSrc, colors } from '../../../../../../constants';

// function SmallPhotos() {
//   const classes = useStyles();
//   return (
//     <Hidden mdUp>
//       <Grid container spacing={3} direction='column' justify='space-around' alignItems='center'>
//         <Grid item xs={12} className={classes.overflowGrid}>
//           <img src={iconsSrc.dummyPillow} height='220px' width='220px'/>
//         </Grid>
//         <Grid item xs={10} style={{ textAlign: 'center' }}>
//           <Carousel/>
//         </Grid>
//       </Grid>
//     </Hidden>
//   )
// }


function MediumPhotos() {
  const classes = useStyles();
  return (
    <Hidden smDown>
      <Grid container direction='row' justify='space-around' alignItems='center'>
        <Grid item md={3} className={classes.overflowGrid}>
          <div>
            <img src={iconsSrc.dummyPillow2} height='80px' width='80px' className={classes.imgs} />
            <img src={iconsSrc.dummyPillow2} height='80px' width='80px' className={classes.imgs} />
          </div>
          <div>
            <img src={iconsSrc.dummyPillow2} height='80px' width='80px' className={classes.imgs} />
            <img src={iconsSrc.dummyPillow2} height='80px' width='80px' className={classes.imgs} />
          </div>
          <div>
            <img src={iconsSrc.dummyPillow2} height='80px' width='80px' className={classes.imgs} />
            <img src={iconsSrc.dummyPillow2} height='80px' width='80px' className={classes.imgs} />
          </div>
          <div>
            <img src={iconsSrc.dummyPillow2} height='80px' width='80px' className={classes.imgs} />
            <img src={iconsSrc.dummyPillow2} height='80px' width='80px' className={classes.imgs} />
          </div>
        </Grid>
        <Grid item md={6} style={{ textAlign: 'center' }}>
          <img src={iconsSrc.dummyPillow} height='280px' width='280px' />
        </Grid>
      </Grid>
    </Hidden>
  )
}


function Photos() {
  const classes = useStyles();
  const [isOpen, setIsOpen] = React.useState(false);

  const handleOpen = () => {
    setIsOpen(true);
  }

  const handleClose = () => {
    setIsOpen(false);
  }

  return (
    <CardContent>
      <Grid container direction='row' justify='space-between' alignItems='center'>
        <Grid item>
          <h2>IMAGENS DO PRODUTO</h2>
        </Grid>
        <Grid item>
          <Button variant='outlined' onClick={handleOpen}><b>Upload</b></Button>
        </Grid>
      </Grid>
      {/* <SmallPhotos/> */}
      <MediumPhotos />
      <Modal open={isOpen} onClose={handleClose}>
        <div className={classes.modal}>
          
        </div>
      </Modal>
    </CardContent>
  )
}

export default Photos;

const useStyles = makeStyles(theme => ({
  imgs: {
    margin: '5px 10px',
    cursor: 'pointer'
  },
  overflowGrid: {
    textAlign: 'right',
    height: 300,
    overflow: 'auto'
  },
  modal: {
    width: '40%',
    height: '50%',
    backgroundColor: colors.transparent06,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 4),
    outline: 'none',
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center'
  }
}))