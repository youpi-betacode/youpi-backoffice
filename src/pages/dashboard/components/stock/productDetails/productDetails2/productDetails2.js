import React, { useEffect } from 'react';
import { Grid, Card, Modal } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';
import { push } from 'connected-react-router';

import Loader from '../../../common/loading';
import ProductInNumbers from './productInNumbers';
import GeneralInfo from './generalInfo';
import Variants from './variants';
import ProductProperties from './productProperties';
import Photos from './photos';
import Footer from '../../../common/footer';
import CustomButton from '../../../common/button';
import ModalContent from '../../../common/modalContent';
import { saveEdits, getProductVariants, toggleDesc, setDesc, setOrder, setPage, setPageSize, getProduct, toggleDeleteProductModal, setCurrentId, deleteProduct } from '../../../../../../redux/actions/productActions';
import { colors } from '../../../../../../constants';
import ProductNotFound from '../productNotFound';

function ProductDetails(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const productId = props.match.params.id;
  const product = useSelector(state => state.product);

  const handleHeaderClick = (label) => {
    if (product.orderBy === label) {
      dispatch(toggleDesc());
    }
    else {
      dispatch(setDesc());
      dispatch(setOrder(label));
    }
  }

  const handleChangePage = (event, newPage) => {
    dispatch(setPage(newPage + 1));
  }

  const handleChangeRowsPerPage = (event) => {
    dispatch(setPageSize(event.target.value))
  }

  const handleModalClose = (toggle) => {
    dispatch(toggleDeleteProductModal(false));
  }

  const handleOnDeleteClick = () => {
    dispatch(toggleDeleteProductModal(true))
  }

  const handleOnCancelClick = () => {
    dispatch(push('/stock'));
  }

  const handleSaveEditClick = () => {
    const formData = new FormData;
    formData.append('name', product.name);
    formData.append('description', product.description);
    formData.append('category', product.category);
    formData.append('store', product.store.id);
    formData.append('supplier', product.supplier.id);
    dispatch(saveEdits(formData, product.id))
  }

  const handleOnRowClick = (id) => {
    dispatch(push(`/variant/${id}`))
  }

  const handleAddClick = (id) => {
    dispatch(push(`/product/${productId}/newvariant`));
  }

  const handleOnNoClick = () => {
    dispatch(toggleDeleteProductModal(false))
  }

  const handleOnYesClick = () => {
    dispatch(deleteProduct(productId));
  }

  useEffect(() => {
    dispatch(getProductVariants(productId, product.page, product.pageSize, product.orderBy, product.desc), dispatch(getProduct(productId), dispatch(setCurrentId(productId))))
  }, [, productId, product.page, product.pageSize, product.orderBy, product.desc, product.editMessage])

  if (!product.exists) {
    return (
      <ProductNotFound />
    )
  }

  return (
    <div>
      <Loader showChildren={false}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Card className={classes.card}>
              <ProductInNumbers productId={productId} />
            </Card>
          </Grid>
          <Grid item xs={12}>
            <Card className={classes.card}>
              <GeneralInfo />
            </Card>
          </Grid>
          <Grid item xs={12}>
            <Card className={classes.card}>
              <ProductProperties />
            </Card>
          </Grid>
          <Grid item xs={12}>
            <Card className={classes.card}>
              <Photos />
            </Card>
          </Grid>
          <Grid item xs={12}>
            <Card className={classes.card}>
              <Variants
                table={product}
                items={product.variants}
                action={id => handleOnRowClick(id)}
                addVariant={id => handleAddClick(id)}
                sortBy={label => handleHeaderClick(label)}
                changePage={(event, newPage) => handleChangePage(event, newPage)}
                changeRowsPerPage={(event) => handleChangeRowsPerPage(event)}
              />
            </Card>
          </Grid>
        </Grid>
        <Card className={clsx(classes.card, classes.footerCard)}>
          <Footer>
            <CustomButton title='apagar produto' action={handleOnDeleteClick} sstyle={classes.footerBtns} />
            <CustomButton title='cancelar' action={handleOnCancelClick} sstyle={classes.footerBtns} />
            <CustomButton title='guardar' action={handleSaveEditClick} sstyle={classes.footerBtns} />
          </Footer>
        </Card>
        <Modal open={product.modal} onClose={handleModalClose}>
          <ModalContent
            text='Deseja apagar este produto?'
            onNoClick={() => handleOnNoClick()}
            onYesClick={() => handleOnYesClick()}
          />
        </Modal>
      </Loader>
    </div>
  )
}

export default ProductDetails;

const useStyles = makeStyles(theme => ({
  card: {
    backgroundColor: colors.transparent02,
    paddingLeft: 10,
    paddingRight: 10
  },
  footerCard: {
    position: 'sticky',
    bottom: 0,
    left: 0,
    marginTop: 20,
    backgroundColor: colors.primary,
    zIndex: 1000
  },
  footerBtns: {
    margin: 'auto 5px'
  }
}))
