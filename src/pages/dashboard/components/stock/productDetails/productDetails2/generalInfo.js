import React from 'react';
import { CardContent, TextField, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useSelector, useDispatch } from 'react-redux';
import { setName, setDescription } from '../../../../../../redux/actions/productActions';
import { colors } from '../../../../../../constants';

function GeneralInfo() {
  const classes = useStyles();
  const product = useSelector(state => state.product);
  const dispatch = useDispatch();

  const handleNameChange = (event) => {
    dispatch(setName(event.target.value));
  }

  const handleDescriptionChange = (event) => {
    dispatch(setDescription(event.target.value));
  }

  return (
    <CardContent>
      <h2>INFORMAÇÕES GERAIS</h2>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <TextField
            label="Nome"
            variant='outlined'
            // className={classes.textField}
            value={product.name}
            onChange={event => handleNameChange(event)}
            margin="normal"
            fullWidth
            // InputLabelProps={{
            //   classes: {
            //     root: classes.labelFocused
            //   }
            // }}
            InputProps={{
              classes: {
                notchedOutline: classes.inputs
              }
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            label="Descrição"
            variant='outlined'
            // className={classes.textField}
            value={product.description}
            onChange={event => handleDescriptionChange(event)}
            margin="normal"
            fullWidth
            multiline
            // InputLabelProps={{
            //   classes: {
            //     root: classes.labelFocused
            //   }
            // }}
            InputProps={{
              classes: {
                notchedOutline: classes.inputs
              }
            }}
          />
        </Grid>
      </Grid>
    </CardContent>
  )
}

export default GeneralInfo;

const useStyles = makeStyles(theme => ({
  inputs: {
    borderRadius: 10,
    // borderColor: colors.white + '!important'
  },
  labelFocused: {
    color: colors.white + '!important'
  }
}))