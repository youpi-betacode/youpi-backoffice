import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { InputBase } from '@material-ui/core';
import { colors } from '../../../../../constants'

function ProductEditLabels() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <div className={classes.labels}>
        <b>
        <InputBase className={classes.input} defaultValue='id de produto' disabled/><br/>
        <InputBase className={classes.input} defaultValue='nome do produto' disabled/><br/>
        <InputBase className={classes.input} defaultValue='categoria do produto' disabled/>
        </b>
      </div>
      <div className={classes.labels}>
        <b>
        <InputBase className={classes.input} defaultValue='etiqueta do produto' disabled/>
        </b>
      </div>
      <div className={classes.labels}>
        <b>
        <InputBase className={classes.input} defaultValue='valor do produto' disabled/><br/>
        <InputBase className={classes.input} defaultValue='peso' disabled/><br/>
        <InputBase className={classes.input} defaultValue='custo transporte' disabled/><br/>
        <InputBase className={classes.input} defaultValue='variacoes' disabled/><br/>
        <InputBase className={classes.input} defaultValue='produtos relacionados' disabled/><br/>
        <InputBase className={classes.input} defaultValue='descricao do produto' disabled/>
        </b>
      </div>
    </React.Fragment>
  )
}

export default ProductEditLabels;

const useStyles = makeStyles(theme => ({
  labels: {
    marginBottom: 30
  },
  input: {
    borderBottom: '2px solid',
    borderBottomColor: 'transparent',
    color: colors.white + '!important',
    padding: '5px 0'
  }
}))
