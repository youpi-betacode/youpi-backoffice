import React from 'react'
import { makeStyles, InputBase } from '@material-ui/core';
import { colors } from '../../../../../constants';
import { useSelector, useDispatch} from 'react-redux';
import { getProduct } from '../../../../../redux/actions/productActions';

function ProductEdit() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const productId = useSelector(state => state.product.id);
  const productCategory = useSelector(state => state.product.category);
  const productDescription = useSelector(state => state.product.description);
  const productName = useSelector(state => state.product.name);
  
  dispatch(getProduct(productId))

  return(
    <React.Fragment>
      <div className={classes.productInfo}>
        <InputBase className={classes.input} defaultValue={productId}/><br/>
        <InputBase className={classes.input} defaultValue={productName}/><br/>
        <InputBase className={classes.input} defaultValue={productCategory}/>
      </div>
      <div className={classes.productInfo}>
      <InputBase className={classes.input} defaultValue='etiqueta do produto'/>
      </div>
      <div className={classes.productInfo}>
        <InputBase className={classes.input} defaultValue='valor do produto'/><br/>
        <InputBase className={classes.input} defaultValue='peso'/><br/>
        <InputBase className={classes.input} defaultValue='custo transporte'/><br/>
        <InputBase className={classes.input} defaultValue='variacoes'/><br/>
        <InputBase className={classes.input} defaultValue='produtos relacionados'/><br/>
        <InputBase className={classes.input} defaultValue={productDescription}/>
      </div>
    </React.Fragment>
  )

}

export default ProductEdit

const useStyles = makeStyles(theme => ({
  productInfo: {
    marginLeft: 50,
    marginBottom: 30
  },
  input: {
    borderBottom: '2px solid',
    borderBottomColor: colors.primary,
    padding: '5px 0'
  }
}))