import React from 'react'
import { Grid } from '@material-ui/core'
import LeftSide from './leftSide';
import RightSide from './rightSide';


function Details() {
  return(
    <Grid 
      container
      direction='row'
      justify='flex-start'
      alignItems='flex-start'
      style={{marginBottom: '40px'}}
    >
      <LeftSide/>
      <RightSide/>
    </Grid>
  )
}

export default Details