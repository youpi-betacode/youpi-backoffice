import React from 'react';
import { Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { colors } from '../../../../../constants';
import { useSelector } from 'react-redux'; 
import ProductInfo from './productInfo';
import ProductLabels from './productLabels';
import ProductEdit from './productEdit';
import ProductEditLabels from './productEditLabels';

function RightSide() {
  const classes = useStyles();
  const editProduct = useSelector(state => state.editProduct.edit);

  if(editProduct){
    return(
      <div>
        <Grid item className={classes.rightGrid}>
          <div className={classes.outerDiv}>
            <div className={classes.col}>
              <ProductEditLabels/>
            </div>
            <div className={classes.col}>
              <ProductEdit/>
            </div>
          </div>
        </Grid>
        <div className={classes.saveBtnDiv}>
          <Button variant='outlined' className={classes.saveBtn}><b>Guardar</b></Button>
        </div>
      </div>
    )
  }

  return (
    <Grid item className={classes.rightGrid}>
      <div className={classes.outerDiv}>
        <div className={classes.col}>
          <ProductLabels/>
        </div>
        <div className={classes.col}>
          <ProductInfo/>
        </div>
      </div>
    </Grid>
  )
}

export default RightSide

const useStyles = makeStyles(theme => ({
  rightGrid: {
    marginLeft: 50
  },
  col: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'column'
  },
  outerDiv: {
    display:'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 50
  },
  saveBtn: {
  },
  saveBtnDiv: {
    textAlign: 'right',
    marginTop: 10
  }
}))