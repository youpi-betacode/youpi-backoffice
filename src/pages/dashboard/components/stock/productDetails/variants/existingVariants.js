import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import { Table, TableBody, TableRow, Typography } from '@material-ui/core';
import { push } from 'connected-react-router';

import { getProductVariants } from '../../../../../../redux/actions/productActions';
import { colors } from '../../../../../../constants';

function ExistingVariants(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const product = useSelector(state => state.product);

  useEffect(() => {
    dispatch(getProductVariants(product.id, 1, 25, 'id', false))
  }, [product.id]);

  return (
    <div>
      <h2 className={classes.header}>Variantes</h2>
      <Table>
        <TableBody>
        {product.variants.map(variant => (
          <TableRow key={variant.id} onClick={() => props.rowClick(variant.id)} className={classes.row}>
            <Typography variant='h6' className={classes.label}>
              {variant.label}
            </Typography>
          </TableRow>
        ))}
        </TableBody>
      </Table>
    </div>
  )
}

export default ExistingVariants;

const useStyles = makeStyles(theme => ({
  label: {
    marginBottom: 5,
    marginTop: 5,
    textAlign: 'center'
  },
  header: {
    textAlign: 'center'
  },
  row: {
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: colors.transparent01
    }
  }
}))