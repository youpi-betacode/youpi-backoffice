import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { CardContent, TextField, InputAdornment } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { setPrice } from '../../../../../../redux/actions/variantsActions';

function Price() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const variant = useSelector(state => state.variant);

  const handlePriceChange = (event) => {
    dispatch(setPrice(event.target.value))
  }

  return (
    <CardContent>
      <h2>Preço</h2>
      <TextField
        type='number'
        label="Preço"
        variant='outlined'
        // className={classes.textField}
        value={variant.price}
        onChange={event => handlePriceChange(event)}
        margin="normal"
        fullWidth
        InputProps={{
          classes: {
            notchedOutline: classes.inputs
          },
          startAdornment: <InputAdornment position='start'>€</InputAdornment>
        }}
      />
    </CardContent>
  )
}

export default Price;

const useStyles = makeStyles(theme => ({

  inputs: {
    borderRadius: 10,
    //borderColor: colors.white + '!important'
  },
}))