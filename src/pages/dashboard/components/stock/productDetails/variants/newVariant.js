import React, { useEffect } from 'react';
import { Grid, Card, CardContent, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';

import ExistingVariants from './existingVariants';
import GeneralInfo from './generalInfo';
import Price from './price';
import Stock from './stock';
import Footer from '../../../common/footer';
import { setCurrentId } from '../../../../../../redux/actions/productActions';
import { addNewVariant } from '../../../../../../redux/actions/variantsActions';
import { colors } from '../../../../../../constants';


function NewVariant(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const productId = props.match.params.id;
  const variant = useSelector(state => state.variant);

  const handleOnCancelClick = () => {
    dispatch(push(`/product/${productId}`));
  }

  const handleOnSaveEditClick = () => {
    const formData = new FormData;
    const stock_status = variant.stock > 0 ? 'Em Stock' : 'Esgotado'; 

    formData.append('reference', 'ref1');
    formData.append('label', variant.label);
    formData.append('stock', variant.stock);
    formData.append('stock_status', stock_status);
    formData.append('price_base', variant.price);

    dispatch(addNewVariant(formData,productId))
  }

  const handleOnRowClick = (id) => {
    dispatch(push(`/variant/${id}`));
  }

  useEffect(() => {
    dispatch(setCurrentId(productId))
  },[productId])

  return (
    <div>
      <Grid container>
        <Grid item xs={12}>
          <Card className={classes.title}>
            <h1>Adicionar nova variante</h1>
          </Card>
        </Grid>
      </Grid>
      <Grid container spacing={3} className={classes.infoContainer}>
        <Grid item xs={12} md={2}>
          <Card className={classes.card}>
            <ExistingVariants 
              rowClick={(id) => handleOnRowClick(id)}
            />
          </Card>
        </Grid>
        <Grid item xs={12} md={10}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Card className={classes.card}>
                <GeneralInfo />
              </Card>
            </Grid>
            <Grid item xs={12}>
              <Card className={classes.card}>
                <Price />
              </Card>
            </Grid>
            <Grid item xs={12}>
              <Card className={classes.card}>
                <Stock />
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Card className={clsx(classes.card, classes.footerCard)}>
        <Footer>
          <CardContent style={{ transform: 'translateY(7%)' }}>
            <Grid container spacing={1}>
              <Grid item md={6} style={{ textAlign: 'left' }}>
                <Button className={classes.footerBtns} variant='outlined' onClick={handleOnCancelClick}>cancelar</Button>
              </Grid>
              <Grid item md={6} style={{ textAlign: 'right' }}>
                <Button className={classes.footerBtns} variant='outlined' onClick={handleOnSaveEditClick}>Guardar</Button>
              </Grid>
            </Grid>
          </CardContent>
        </Footer>
      </Card>
    </div>
  )
}

export default NewVariant;

const useStyles = makeStyles(theme => ({
  title: {
    paddingLeft: 10,
    backgroundColor: 'transparent',
    boxShadow: 'none'
  },
  card: {
    backgroundColor: colors.transparent06,
  },
  footerCard: {
    position: 'sticky',
    bottom: 0,
    left: 0,
    marginTop: 20,
    backgroundColor: colors.primary,
    zIndex: 1000
  },
  footerBtns: {
    margin: 'auto 5px'
  },
  infoContainer: {
    marginTop: 20
  }
}))