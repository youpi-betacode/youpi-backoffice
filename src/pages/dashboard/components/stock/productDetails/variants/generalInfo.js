import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import { CardContent, TextField} from '@material-ui/core';

import { setLabel } from '../../../../../../redux/actions/variantsActions';
import { colors } from '../../../../../../constants';

function GeneralInfo() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const variant = useSelector(state => state.variant);

  const handleLabelChange = (event) => {
    dispatch(setLabel(event.target.value))
  }

  return (
    <CardContent>
      <h2>Informação Geral</h2>
      <TextField
            label="Label"
            variant='outlined'
            // className={classes.textField}
            value={variant.label}
            onChange={event => handleLabelChange(event)}
            margin="normal"
            fullWidth
            // InputLabelProps={{
            //   classes: {
            //     root: classes.labelFocused
            //   }
            // }}
            InputProps={{
              classes: {
                notchedOutline: classes.inputs
              }
            }}
          />
    </CardContent>
  )
}

export default GeneralInfo;

const useStyles = makeStyles(theme => ({

  inputs: {
    borderRadius: 10,
    //borderColor: colors.white + '!important'
  },
  labelFocused: {
    color: colors.white + '!important'
  }

}))