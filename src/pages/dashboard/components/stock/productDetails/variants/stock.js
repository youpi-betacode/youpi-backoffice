import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { CardContent, TextField } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';

import { setStock } from '../../../../../../redux/actions/variantsActions';

function Stock() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const variant = useSelector(state => state.variant);
  
  const handleStockChange = (event) => {
    dispatch(setStock(event.target.value))
  }
  
  return (
    <CardContent>
      <h2>Stock</h2>
      <TextField
        type='number'
        label="Stock"
        variant='outlined'
        // className={classes.textField}
        value={variant.stock}
        onChange={event => handleStockChange(event)}
        margin="normal"
        fullWidth
        InputProps={{
          classes: {
            notchedOutline: classes.inputs
          }
        }}
      />
    </CardContent>
  )
}

export default Stock;

const useStyles = makeStyles(theme => ({

  inputs: {
    borderRadius: 10,
    //borderColor: colors.white + '!important'
  },
}))