import React, { useEffect, useState } from 'react';
import { Grid, Card, CardContent, Button, Modal } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import { push } from 'connected-react-router';

import ExistingVariants from './existingVariants';
import GeneralInfo from './generalInfo';
import Price from './price';
import Stock from './stock';
import Footer from '../../../common/footer';
import CustomButton from '../../../common/button';
import ModalContent from '../../../common/modalContent';
import { colors } from '../../../../../../constants';
import { getVariant, deleteVariant, saveVariantEdit } from '../../../../../../redux/actions/variantsActions';
import { setCurrentId } from '../../../../../../redux/actions/productActions';

function EditVariant(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const variant = useSelector(state => state.variant);
  const currentVariantId = props.match.params.id;
  const [modal,setModal] = useState(false);

  const handleOnDeleteClick = () => {
    setModal(true);
  }

  const handleOnCancelClick = () => {
    dispatch(push(`/product/${variant.productId}`));
  }

  const handleOnSaveEditClick = () => {
    const formData = new FormData;
    formData.append('reference', variant.reference);
    formData.append('label', variant.label);
    formData.append('stock_status', variant.stockStatus);
    formData.append('price_base', variant.price);
    dispatch(saveVariantEdit(formData,currentVariantId))
  }

  const handleOnRowClick = (id) => {
    dispatch(push(`/variant/${id}`));
  }

  const handleOnNoClick = () => {
    setModal(false);
  }

  const handleOnYesClick = (id) => {
    dispatch(deleteVariant(currentVariantId));
    setModal(false);
  }

  useEffect(() => {
    dispatch(getVariant(currentVariantId), dispatch(setCurrentId(variant.productId)))
  }, [currentVariantId, variant.productId]);

  return (
    <div>
      <Grid container>
        <Grid item xs={12}>
          <Card className={classes.title}>
            <h1>{variant.label}</h1>
          </Card>
        </Grid>
      </Grid>
      <Grid container spacing={3} className={classes.infoContainer}>
        <Grid item xs={12} md={2}>
          <Card className={classes.card}>
            <ExistingVariants 
              rowClick={(id) => handleOnRowClick(id)}
              currentVariantId={currentVariantId}
            />
          </Card>
        </Grid>
        <Grid item xs={12} md={10}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Card className={classes.card}>
                <GeneralInfo />
              </Card>
            </Grid>
            <Grid item xs={12}>
              <Card className={classes.card}>
                <Price />
              </Card>
            </Grid>
            <Grid item xs={12}>
              <Card className={classes.card}>
                <Stock />
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Card className={clsx(classes.card, classes.footerCard)}>
        <Footer>
          <CustomButton title='apagar variante' action={handleOnDeleteClick} sstyle={classes.footerBtns}/>
          <CustomButton title='cancelar' action={handleOnCancelClick} sstyle={classes.footerBtns}/>
          <CustomButton title='guardar' action={handleOnSaveEditClick} sstyle={classes.footerBtns}/>
        </Footer>
      </Card>
      <Modal open={modal} onClose={handleOnNoClick}>
        <ModalContent
        text='Deseja apagar esta variante?'
        onNoClick={() => handleOnNoClick()}
        onYesClick={(id) => handleOnYesClick(id)}
        />
      </Modal>
    </div>
  )
}

export default EditVariant;

const useStyles = makeStyles(theme => ({
  card: {
    backgroundColor: colors.transparent06,
  },
  infoContainer: {
    marginTop: 20
  },
  title: {
    paddingLeft: 10,
    backgroundColor: 'transparent',
    boxShadow: 'none'
  },
  footerCard: {
    position: 'sticky',
    bottom: 0,
    left: 0,
    marginTop: 20,
    backgroundColor: colors.primary,
    zIndex: 1000
  },
  footerBtns: {
    margin: 'auto 5px'
  }
}))