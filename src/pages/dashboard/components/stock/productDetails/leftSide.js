import React from 'react'
import { Grid, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles';
import { iconsSrc, colors } from '../../../../../constants';
import Carousel from './carousel';
import { useSelector } from 'react-redux'

function LeftSide() {
  const classes = useStyles();

  const displayedImg = useSelector(state => state.product.selectedImage);

  return (
    <Grid item className={classes.leftGrid}>
      <div>
        <h2>Nome do Produto</h2>
        <img src={displayedImg} height='380' width='380' alt='Imagem do Produto'/>
      </div>
      <br/>
      <div style={{maxWidth: 380}}>
        <Carousel/>
      </div>
      <div style={{textAlign: 'center', marginTop: 20}}>
        <label>Galeria de Fotos</label><br/>
        <Button variant='outlined'><b>Upload</b></Button>
      </div>
      <div style={{textAlign: 'center', marginTop: 30}}>
        <label>PRODUTO EM NÚMEROS </label><br/><br/>
        <div className={classes.circlesDiv}>
          <div>
            <div className={classes.numberDivs}>
              43
            </div>
            Nº de Vendas
          </div>
          <div>
            <div className={classes.numberDivs}>
              943
            </div>
            Valor de Vendas
          </div>
          <div>
            <div className={classes.numberDivs}>
              5000
            </div>
            Nº de Compradores
          </div>
        </div>
      </div>
    </Grid>
  )
}

export default LeftSide

const useStyles = makeStyles(theme => ({
  leftGrid: {
    paddingLeft: 30
  },
  uploadBtn: {
    // border: '1px solid',
    // borderColor: colors.white,
    // textTransform: 'capitalize',
    // padding: '0 15px',
    //margin: '10px auto'
  },
  numberDivs: {
    backgroundColor: colors.lightGrey,
    color: colors.black,
    border: '1px solid',
    borderColor: colors.lightGrey,
    width: 100,
    height: 100,
    borderRadius: '50%',
    textAlign: 'center',
    lineHeight: '100px',
    fontSize: 22,
    marginBottom: 10,
    marginLeft: 10
  },
  circlesDiv: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row'
  }
}))