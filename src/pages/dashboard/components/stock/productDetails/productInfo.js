import React from 'react'
import { makeStyles } from '@material-ui/styles';
import { Button } from '@material-ui/core';
import { colors } from '../../../../../constants';
import { useSelector, useDispatch } from 'react-redux';
import { getProduct } from '../../../../../redux/actions/productActions';

function ProductInfo(props) {
  const classes = useStyles();
  const dispatch = useDispatch();

  const productId = useSelector(state => state.product.id);
  const productCategory = useSelector(state => state.product.category);
  const productDescription = useSelector(state => state.product.description);
  const productName = useSelector(state => state.product.name);
  
  dispatch(getProduct(productId))
  
  return (
    <React.Fragment>
      <div className={classes.productInfo}>
        <p>{productId}</p>
        <p>{productName}</p>
        <p>{productCategory}</p>
      </div>
      <div className={classes.productInfo}>
        <p>etiqueta do produto</p>
      </div>
      <div className={classes.productInfo}>
        <p>valor do produto</p>
        <p>peso</p>
        <p>custo transporte</p>
        <p>variacoes</p>
        <p>produtos relacionados</p>
        <p>{productDescription}</p>
      </div>
      <div className={classes.productInfo}>
        <Button variant='outlined' className={classes.promotionBtn}><b>Criar Promoção</b></Button>
      </div>
    </React.Fragment>
  )
}

export default ProductInfo

const useStyles = makeStyles(theme => ({
  productInfo: {
    marginLeft: 50
  },
  promotionBtn: {
    // border: '1px solid',
    // borderColor: colors.white,
    // textTransform: 'capitalize',
    // padding: '0 15px',
    // margin: '10px auto'
  }
}))