import React from 'react';
import { Grid, makeStyles } from '@material-ui/core';

function ProductLabels() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <div>
        <b>
          <p>ID DE PRODUTO:</p>
          <p>NOME DO PRODUTO:</p>
          <p>CATEGORIA DO PRODUTO:</p>
        </b>
      </div>
      <div>
        <b>
          <p>ETIQUETA DE PRODUTO:</p>
        </b>
      </div>
      <div>
        <b>
          <p>VALOR DO PRODUTO:</p>
          <p>PESO:</p>
          <p>CUSTO TRANSPORTE:</p>
          <p>VARIAÇÕES:</p>
          <p>PRODUTOS RELACIONADOS:</p>
          <p>DESCRIÇÃO DO PRODUTO:</p>
        </b>
      </div>
      <div>
        <b>
          <p>PROMOÇÃO:</p>
        </b>
      </div>
    </React.Fragment>
  )
}

export default ProductLabels;

const useStyles = makeStyles(theme => ({
}))