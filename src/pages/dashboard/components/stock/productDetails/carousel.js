import React from 'react'
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { makeStyles } from '@material-ui/styles';
import { iconsSrc } from '../../../../../constants';
import { useDispatch } from 'react-redux';
import { setDisplayedImage } from '../../../../../redux/actions/productActions';

// const settings = {
//   infinite: true,
//   slidesToShow: 4,
//   slidesToScroll: 3
// };

const settings = {
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  swipeToSlide: true
};

//todo: still static, fetch from backend
  function Carousel(){

    const dispatch = useDispatch();

    const handlePictureClick = (e) => {
      dispatch(setDisplayedImage(e.target.src))
    }

    const classes = useStyles();
    return (
      <Slider {...settings}>
        <div className={classes.imgDiv}>
          <img src={iconsSrc.dummyPillow} height='80' width='80' alt='Imagem do Produto' className={classes.images} onClick={(e) => handlePictureClick(e)}/>
        </div>
        <div className={classes.imgDiv}>
          <img src={iconsSrc.dummyPillow2} height='80' width='80' alt='Imagem do Produto' className={classes.images} onClick={(e) => handlePictureClick(e)}/>
        </div>
        <div className={classes.imgDiv}>
          <img src={iconsSrc.dummyPillow} height='80' width='80' alt='Imagem do Produto' className={classes.images} onClick={(e) => handlePictureClick(e)}/>
        </div>
        <div className={classes.imgDiv}>
          <img src={iconsSrc.dummyPillow2} height='80' width='80' alt='Imagem do Produto' className={classes.images} onClick={(e) => handlePictureClick(e)}/>
        </div>
        <div className={classes.imgDiv}>
          <img src={iconsSrc.dummyPillow} height='80' width='80' alt='Imagem do Produto' className={classes.images} onClick={(e) => handlePictureClick(e)}/>
        </div>
        <div className={classes.imgDiv}>
          <img src={iconsSrc.dummyPillow2} height='80' width='80' alt='Imagem do Produto' className={classes.images} onClick={(e) => handlePictureClick(e)}/>
        </div>
      </Slider>
    )
  }

  export default Carousel;

  const useStyles = makeStyles(theme => ({
    images: {
      cursor: 'pointer'
    },
    imgDiv: {
      outlineStyle: 'none'
    }
  }))