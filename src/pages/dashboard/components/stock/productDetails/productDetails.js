import React from 'react'
import { Paper } from '@material-ui/core'
import CustomButton from '../../common/button'
import Searchbar from '../../common/searchbar'
import BackBar from '../../common/backBar'
import Details from './details';
import { colors } from '../../../../../constants'
import { useDispatch, useSelector } from 'react-redux'
import { setCurrentId, getProduct } from '../../../../../redux/actions/productActions';
import ProductNotFound from './productNotFound'

function ProductDetails(props) {
  const dispatch = useDispatch();

  const productId = props.match.params.id;

  dispatch(setCurrentId(productId));
  dispatch(getProduct(productId));

  const hasProduct = useSelector(state => state.product.exists);

  return(
    <div style={{width: '100%'}}>
      <Paper style={{backgroundColor: colors.transparent02}}>
        <Searchbar title='Gestão de Stock'>
          <CustomButton title='Adicionar Produto'/>
          <CustomButton title='Exportar Report'/>
        </Searchbar>
        <BackBar/>
        {hasProduct ? <Details/> : <ProductNotFound/>}
      </Paper>
    </div>
  )

}

export default ProductDetails