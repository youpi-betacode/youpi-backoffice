import React from 'react';
import { Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { colors } from '../../../../constants';

function ModalContent(props) {
  const classes = useStyles();
  return (
    <div className={classes.modal}>
      <h2>{props.text}</h2>
      <Grid container direction='row' justify='space-around' alignItems='center'>
        <Grid item xs={12} md={4} className={classes.gridItem}>
          <Button variant='outlined' fullWidth onClick={(id) => props.onYesClick(id)}>Sim</Button>
        </Grid>
        <Grid item xs={12} md={4} className={classes.gridItem}>
          <Button variant='outlined' fullWidth onClick={() => props.onNoClick()}>Não</Button>
        </Grid>
      </Grid>
    </div>
  )
}

export default ModalContent;

const useStyles = makeStyles(theme => ({
  modal: {
    backgroundColor: colors.transparent06,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 4),
    outline: 'none',
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center'
  },
  gridItem: {
    marginTop: 10
  }
}))