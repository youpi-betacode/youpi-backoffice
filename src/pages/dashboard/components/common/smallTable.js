import React from 'react';
import { useDispatch } from 'react-redux'
import { Table, TableBody, TableRow, TableCell, Paper, Grid, TablePagination } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { colors, rowsPerPageOptions } from '../../../../constants';

function Small(props) {
  const classes = useStyles();

  const handleOnClick = (id) => {
    if(props.table.hover)
      props.action(id)
  }

  // useEffect(() => {
  //   dispatch(getProducts(token, page, pageSize, orderBy, desc, category, supplier))
  // }, [orderBy, desc, category, supplier, page, pageSize])

  return (
    <Grid container>
      <Grid item xs>
        <Paper style={{ width: '100%', height: '100%', overflow: 'auto' }}>
          {props.items.map(product => (
            <Table className={props.table.hover ? classes.table : classes.defaultCursorTable}
              key={product.id}
              onClick={() => handleOnClick(product.id)}
                >
              {props.table.labels.map(label => (
                <TableBody key={label.key + product.id}>
                  <TableRow className={classes.row}>
                    <TableCell align="left">{label.value}</TableCell>
                    <TableCell align="right">{eval(`product.${label.key}`)}</TableCell>
                  </TableRow>
                </TableBody>
              ))}
            </Table>
          ))}
          <TablePagination
            rowsPerPage={props.table.pageSize}
            rowsPerPageOptions={rowsPerPageOptions}
            count={props.table.count}
            page={props.table.page - 1}
            onChangePage={(event, newPage) => props.changePage(event, newPage)}
            onChangeRowsPerPage={(event) => props.changeRowsPerPage(event)}
          />
        </Paper>
      </Grid>
    </Grid>
  )

}

export default Small;

const useStyles = makeStyles(theme => ({
  table: {
    '&:nth-of-type(even)': {
      backgroundColor: colors.transparent04,
    },
    '&:nth-of-type(odd)': {
      backgroundColor: colors.transparent02,
    },
    margin: '5px 0px',
    cursor: 'pointer'
  },
  defaultCursorTable: {
    '&:nth-of-type(even)': {
      backgroundColor: colors.transparent04,
    },
    '&:nth-of-type(odd)': {
      backgroundColor: colors.transparent02,
    },
    margin: '5px 0px',
    cursor: 'default'
  },
  row: {
    borderBottom: '1px solid black'
  }
}));