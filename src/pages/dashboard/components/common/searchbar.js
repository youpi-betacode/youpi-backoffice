import React from 'react'
import { Icon, Typography, Button, Grid, TextField, Hidden, InputBase } from '@material-ui/core';
import { fade, makeStyles } from '@material-ui/core/styles';
import { colors } from '../../../../constants'

const handleOnSearchChange = (e) => {
}

function ExtraSmall(props) {
  const classes = useStyles();
  return (
    <Hidden smUp>
      <Grid container>
        <Grid item xs={12} className={classes.extraSmall}>
          <Typography className={classes.title} variant="h5">
            {props.title}
          </Typography>
        </Grid>
        <Grid item xs={12} className={classes.extraSmall}>
        <div className={classes.search}>
            <div className={classes.searchIcon}>
              <Icon>search</Icon>
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>

          {/* <TextField
          id="standard-name"
          type="search"
          label="Pesquisar ..."
          className={classes.textField}
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            }
          }}
          InputProps={{
            classes:{
              root: classes.cssOutlinedInput
            }
          }}
          onChange={(e) => handleOnSearchChange(e)}
          /> */}
          {/* <Button className={classes.buttonSearch}>
            <Icon className={classes.searchIcon}>search</Icon>
          </Button> */}
        </Grid>
        {props.child.length && props.child.map((button,index) => (
          <Grid key={index} item xs={12} className={classes.extraSmall}>
            {button}
          </Grid>
        ))}
        {!props.child.length &&
          <Grid item xs={12} className={classes.extraSmall}>
            {props.child}
          </Grid>
        }
        {/* <Grid item xs={}>
          <Button style={{backgroundColor: '#ff0000'}}>o</Button>
        </Grid>
        <Grid item xs={4}>
          <Button style={{backgroundColor: '#0000ff'}}>o</Button>
        </Grid> */}
      </Grid>
    </Hidden>
  )
}

function Medium(props) {
  const classes = useStyles();
  return (
    <Hidden xsDown>
      <Grid item md={props.rigth ? props.rigth : 4}>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <Icon>search</Icon>
          </div>
          <InputBase
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={{ 'aria-label': 'search' }}
          />
        </div>
        {/* <TextField
          id="standard-name"
          type="search"
          label="Pesquisar ..."
          className={classes.textField}
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            }
          }}
          InputProps={{
            classes:{
              root: classes.cssOutlinedInput
            }
          }}
          onChange={(e) => handleOnSearchChange(e)}
          /> */}
        {/* <Button className={classes.buttonSearch}>
          <Icon className={classes.searchIcon}>search</Icon>
        </Button> */}
      </Grid>
      <Grid item md={props.center ? props.center : 4} className={classes.centerGrid}>
        <Typography className={classes.title} variant="h5">
          {props.title}
        </Typography>
      </Grid>
      <Grid item md={props.right ? props.right : 4} className={classes.rightGrid}>
        {props.child}
      </Grid>
    </Hidden>
  )
}

function Searchbar(props) {
  const classes = useStyles();
  return (
    <Grid
      container
      alignItems="center"
      className={classes.root}
    >
      <ExtraSmall title={props.title} child={props.children} />
      <Medium title={props.title} child={props.children} right={props.right} center={props.center} left={props.left} />
    </Grid>
  )
}

export default Searchbar

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: colors.white,
    padding: '5px 10px'
  },
  // textField: {
  //   width: 200,
  //   borderBottom: '0px',
  //   marginLeft: theme.spacing(1)
  // },
  // buttonSearch: {
  //   padding: theme.spacing(1)
  // },
  title: {
    color: colors.primary,
    fontWeight: 600,
    fontSize: 20,
    margin: '3px auto'
  },
  cssOutlinedInput: {
    marginBottom: 10,
    color: colors.black,
    borderBottom: '1px solid',
    borderColor: colors.primary + '!important'
  },
  cssLabel: {
    color: colors.lightGrey
  },
  centerGrid: {
    textAlign: 'center'
  },
  rightGrid: {
    textAlign: 'right',
  },
  extraSmall: {
    textAlign: 'center',
    margin: '2px auto',
    
  },
  inputRoot: {
    color: colors.black
  },
  search: {
    position: 'relative',
    margin: '3px auto',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(colors.loginGrey, 0.8),
    '&:hover': {
      backgroundColor: fade(colors.lightGrey, 1),
    }
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: colors.black
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      }
    }
  }
}))