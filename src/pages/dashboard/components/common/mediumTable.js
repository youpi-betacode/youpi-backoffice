import React from 'react';
import { Table, TableHead, TableRow, TableCell, TableSortLabel, TableBody, TablePagination } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { rowsPerPageOptions, colors } from '../../../../constants';

function MediumTable(props) {
  const classes = useStyles();

  const handleOnClick = (id) => {
    if(props.table.hover)
      props.action(id)
  }

  return (
    <React.Fragment>
      <Table>
        <TableHead>
          <TableRow>
            {props.table.labels.map((label,index) => (
              <TableCell key={index}>
                <TableSortLabel
                  children={label.value}
                  active={props.table.orderBy === label.key}
                  direction={props.table.desc === '' ? 'desc' : 'asc'}
                  onClick={() => props.sortBy(label.key)}
                />
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items.map(product => (
            <TableRow key={product.id}
              onClick={() => handleOnClick(product.id)}
              className={props.table.hover ? classes.hoverCursorRow : ''}
            >
                {props.table.labels.map((label,index) => (
                  <TableCell key={index}>{eval(`product.${label.key}`)}</TableCell>
                ))}
              </TableRow>
          ))}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPage={props.table.pageSize}
        rowsPerPageOptions={rowsPerPageOptions}
        count={props.table.count}
        page={props.table.page - 1}
        onChangePage={(event, newPage) => props.changePage(event, newPage)}
        onChangeRowsPerPage={(event) => props.changeRowsPerPage (event)}
      />
    </React.Fragment>
  )
}

export default MediumTable;

const useStyles = makeStyles(theme => ({
  hoverCursorRow: {
    cursor: 'pointer',
    '&:hover':{
      backgroundColor: colors.transparent01
    }
  }
}))