import React from 'react'
import { Button, Hidden } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../../../constants'

export default props => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Hidden xsDown>
        <Button
          variant="outlined"
          onClick={props.action}
          className={props.sstyle ? props.sstyle : classes.button}>
          <b>{props.title}</b>
        </Button>
      </Hidden>
      <Hidden smUp>
        <Button
          variant="outlined"
          onClick={props.action}
          style={{ width: '100%', marginLeft: 0 }}
          className={props.sstyle ? props.sstyle : classes.button}>
          <b>{props.title}</b>
        </Button>
      </Hidden>
    </React.Fragment>
  )
}

const useStyles = makeStyles(theme => ({
  button: {
    textTransform: 'capitalize',
    color: colors.black,
    marginLeft: theme.spacing(1),
    fontSize: 12,
    margin: '5px 0px',
    border: '1px solid ' + colors.primary
  }
}))
