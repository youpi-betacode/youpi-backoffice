import React from 'react'
import { Hidden } from '@material-ui/core';

import MediumTable from './mediumTable';
import SmallTable from './smallTable';

function Table(props) {
  return(
    <React.Fragment>
      <Hidden smDown>
        <MediumTable
          table={props.table}
          items={props.items}
          action={(id) => props.action(id)}
          sortBy={(label) => props.sortBy(label)}
          changePage={(event, newPage) => props.changePage(event, newPage)}
          changeRowsPerPage={(event) => props.changeRowsPerPage (event)}
        />
      </Hidden>
      <Hidden mdUp>
        <SmallTable
          table={props.table}
          items={props.items}
          action={id => props.action(id)}
          changePage={(event, newPage) => props.changePage(event, newPage)}
          changeRowsPerPage={(event) => props.changeRowsPerPage (event)}
        />
      </Hidden>
    </React.Fragment>
  )
}

export default Table;