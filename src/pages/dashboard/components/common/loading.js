import React from 'react'
import { CircularProgress, makeStyles } from '@material-ui/core'
import { useSelector } from 'react-redux'
import BlockUi from 'react-block-ui'
import 'react-block-ui/style.css';
import './loadingStyle.css';

function Loading(props){
  const loading = useSelector(state => state.loading.loading);

  return (
    <BlockUi blocking={loading} loader={<CircularProgress/>} renderChildren={props.showChildren}>
      {props.children}
    </BlockUi>
  )
}

export default Loading;