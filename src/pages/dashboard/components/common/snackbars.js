import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Snackbar, Icon } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { toggleInvalidFileSnackbar, toggleSuccessfullUploadFile } from '../../../../redux/actions/maillingListActions';
import { toggleSuccessEdit, toggleSuccessDeletion, toggleUnsuccessDeletion, toggleUnsuccessEdit, toggleUnsuccessAdd, toggleSuccessAdd } from '../../../../redux/actions/editProductActions';
import { toggleSuccessAddVariant, toggleUnsuccessAddVariant, toggleSuccessDeleteVariant, toggleUnsuccessDeleteVariant, toggleSuccessEditVariant, toggleUnsuccessEditVariant } from '../../../../redux/actions/variantsActions';

import { colors } from '../../../../constants';

function Snackbars() {
  const styles = useStyles();
  const dispatch = useDispatch();

  const validFile = useSelector(state => state.maillingList.validFile);
  const successUpload = useSelector(state => state.maillingList.successUpload);
  const fileError = useSelector(state => state.maillingList.error);
  const editProduct = useSelector(state => state.editProduct);
  const variant = useSelector(state => state.variant);


  const handleOnCloseErrorSnackbar = () => {
    dispatch(toggleInvalidFileSnackbar(true));
  }

  const handleOnCloseSuccessSnackbar = () => {
    dispatch(toggleSuccessfullUploadFile());
  }

  const handleOnCloseSuccessProductDeletion = () => {
    dispatch(toggleSuccessDeletion(false));
  }

  const handleOnCloseUnsuccessProductDeletion = () => {
    dispatch(toggleUnsuccessDeletion(false));
  }

  const handleOnCloseSuccessProductEdit = () => {
    dispatch(toggleSuccessEdit(false));
  }
  
  const handleOnCloseUnsuccessProductEdit = () => {
    dispatch(toggleUnsuccessEdit(false));
  }

  const handleOnCloseSuccessAdd = () => {
    dispatch(toggleSuccessAdd(false));
  }

  const handleOnCloseUnsuccessAdd = () => {
    dispatch(toggleUnsuccessAdd(false));
  }

  const handleOnCloseSuccessAddVariant = () => {
    dispatch(toggleSuccessAddVariant(false));
  }

  const handleOnCloseUnsuccessAddvariant = () => {
    dispatch(toggleUnsuccessAddVariant(false));
  }

  const handleOnCloseSuccessDeleteVariant = () => {
    dispatch(toggleSuccessDeleteVariant(false));
  }

  const handleOnCloseUnsuccessDeletevariant = () => {
    dispatch(toggleUnsuccessDeleteVariant(false));
  }

  const handleOnCloseSuccessEditVariant = () => {
    dispatch(toggleSuccessEditVariant(false));
  }

  const handleOnCloseUnsuccessEditvariant = () => {
    dispatch(toggleUnsuccessEditVariant(false));
  }

  return (
    <React.Fragment>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={!validFile}
        autoHideDuration={6000}
        onClose={handleOnCloseErrorSnackbar}
        message={<span>{fileError}</span>}
        action={[
          <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseErrorSnackbar}>close</Icon>
        ]}
        ContentProps={{
          classes: {
            root: styles.errorSnack
          }
        }}
      />
      <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={successUpload}
          autoHideDuration={6000}
          onClose={handleOnCloseSuccessSnackbar}
          message={<span>Ficheiro submetido com sucesso</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseSuccessSnackbar}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.successSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={editProduct.successDeletion}
          autoHideDuration={6000}
          onClose={handleOnCloseSuccessProductDeletion}
          message={<span>Produto removido com sucesso</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseSuccessProductDeletion}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.successSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={editProduct.unsuccessDeletion}
          autoHideDuration={6000}
          onClose={handleOnCloseUnsuccessProductDeletion}
          message={<span>Não foi possivel remover o produto</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseUnsuccessProductDeletion}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.errorSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={editProduct.successEdit}
          autoHideDuration={6000}
          onClose={handleOnCloseSuccessProductEdit}
          message={<span>Alterações guardadas com sucesso</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseSuccessProductEdit}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.successSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={editProduct.unsuccessEdit}
          autoHideDuration={6000}
          onClose={handleOnCloseUnsuccessProductEdit}
          message={<span>Não foi possivel guardar as alterações</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseUnsuccessProductEdit}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.errorSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={editProduct.successAdd}
          autoHideDuration={6000}
          onClose={handleOnCloseSuccessAdd}
          message={<span>Novo producto adicionado com sucesso</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseSuccessAdd}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.successSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={editProduct.unsuccessAdd}
          autoHideDuration={6000}
          onClose={handleOnCloseUnsuccessAdd}
          message={<span>Ocorreu um erro ao adicionar o produto</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseUnsuccessAdd}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.errorSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={variant.successAdd}
          autoHideDuration={6000}
          onClose={handleOnCloseSuccessAddVariant}
          message={<span>Nova variante adicionada com sucesso</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseSuccessAddVariant}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.successSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={variant.unsuccessAdd}
          autoHideDuration={6000}
          onClose={handleOnCloseUnsuccessAddvariant}
          message={<span>Ocorreu um erro ao adicionar a variante</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseUnsuccessAddvariant}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.errorSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={variant.successDelete}
          autoHideDuration={6000}
          onClose={handleOnCloseSuccessDeleteVariant}
          message={<span>Variante apagada com sucesso</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseSuccessDeleteVariant}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.successSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={variant.unsuccessDelete}
          autoHideDuration={6000}
          onClose={handleOnCloseUnsuccessDeletevariant}
          message={<span>Ocorreu um erro ao eliminar a variante</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseUnsuccessDeletevariant}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.errorSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={variant.successEdit}
          autoHideDuration={6000}
          onClose={handleOnCloseSuccessEditVariant}
          message={<span>Variante editada com sucesso</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseSuccessEditVariant}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.successSnack
            }
          }}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={variant.unsuccessEdit}
          autoHideDuration={6000}
          onClose={handleOnCloseUnsuccessEditvariant}
          message={<span>Ocorreu um erro ao editar a variante</span>}
          action={[
            <Icon style={{cursor: 'pointer'}} onClick={handleOnCloseUnsuccessEditvariant}>close</Icon>
          ]}
          ContentProps={{
            classes: {
              root: styles.errorSnack
            }
          }}
        />
    </React.Fragment>
  )
}

export default Snackbars;

const useStyles = makeStyles(theme => ({
  errorSnack: {
    backgroundColor:  colors.errorRed + ' !important',
    fontSize: 16
  },
  successSnack: {
    backgroundColor: colors.success + '!important',
    fontSize: 16
  }
}))