import React from 'react';
import { colors } from '../../../../constants';
import { Button,Icon } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { push } from 'connected-react-router';
import { useSelector, useDispatch } from 'react-redux'
import { toggleEditProduct } from '../../../../redux/actions/editProductActions'

export default props => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleReturnClick = () => {
    dispatch(push('/stock'))
  }

  const handleEditClick = () => {
    dispatch(toggleEditProduct())
  }

  const edit = useSelector(state => state.editProduct.edit)

  function showEdit(){
    if(edit)
      return (
        <Button className={classes.btn} onClick={handleEditClick}>
          <b>Detalhes do Produto</b>
        </Button>
      )

    return (
      <Button className={classes.btn} onClick={handleEditClick}>
        <Icon>edit</Icon>
        &nbsp;<b>Editar Produto</b>
      </Button>
    )
  }

  return(
    <div className={classes.back}>
      <div>
        <Button className={classes.btn} onClick={handleReturnClick}>
          <Icon>keyboard_arrow_left</Icon>
          <b>VOLTAR</b>
        </Button>
      </div>
      <div>
       {showEdit()}
      </div>
    </div>
  )
}

const useStyles = makeStyles(theme => ({
  back: {
    display: 'flex',
    direction: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 50,
    padding: '0 10px',
    marginBottom: '5px',
    backgroundColor: colors.transparent06
  },
  btn: {
    color:colors.iconGreenish,
    marginTop: 0
}
}))