import React from 'react';
import { CardContent, Grid, Hidden, Button } from '@material-ui/core';

function Footer(props) {

  const rightSideChildren = props.children.slice(1);

  return (
    <CardContent style={{ transform: 'translateY(7%)' }}>
      <Grid container spacing={1}>
        <Grid item xs={12} md={6} style={{ textAlign: 'left' }}>
          {props.children[0]}
        </Grid>
        <Grid item xs={12} md={6} style={{ textAlign: 'right' }}>
          {rightSideChildren}
       </Grid>
      </Grid>
    </CardContent>
  )
}

export default Footer;