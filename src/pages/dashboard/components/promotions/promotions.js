import React from 'react';
import { Table, TableBody, TableCell, TableHead, TableRow, Paper, TableSortLabel } from '@material-ui/core';
import Searchbar from '../common/searchbar';
import TablePages from '../tablePagination'
import { useSelector } from 'react-redux'
import CustomButton from '../common/button'
import Loader from '../common/loading';

function createData(id, name, email, location, products, contract_expiration_date, sales_physical_store, sales_online_store, sales_total) {
    return { id, name, email, location, products, contract_expiration_date, sales_physical_store, sales_online_store, sales_total };
}

const rows = [
    createData(0, 'Henhica', 'henhica@pimbolin.com', 'Henhicolandia', 25, "20/12/2019", 250, 300, 550),
    createData(0, 'Shportin', 'shportin@tranquilidade.pt', 'WC', 25, "20/12/2020", 250, 300, 550),
]

//todo: change onClick action when adding reduce
const handleOnClick = (e) => {
	e.preventDefault();
}

/*todo: length of the products is still passed as props
* to table pagination change when integrating with backend
*/
function Promotions() {

  const nrRows = useSelector(state => state.tablePagination.nrRows);
	const page = useSelector(state => state.tablePagination.currentPage);

  return (
    <div>
      <Paper style={{marginRight: 20}}>
        <Searchbar title='Promoções'>
          <CustomButton title='Adicionar Promoção' action={handleOnClick}/>
          <CustomButton title='Exportar Report' action={handleOnClick}/>
			  </Searchbar>
        <Loader showChildren={true}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  <TableSortLabel direction="asc" children={'PRODUTOS'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel direction="asc" children={'ID PRODUTO'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel direction="asc" children={'EM STOCK'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel direction="asc" children={'PREÇO'}
                  />
                  </TableCell>
                <TableCell>
                  <TableSortLabel direction="asc" children={'CATEGORIA'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel direction="asc" children={'DATA STOCK ATUAL'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel direction="asc" children={'INÍCIO CAMPANHA'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel direction="asc" children={'FIM DE CAMPANHA'}
                  />
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.slice(page*nrRows,page*nrRows+nrRows).map(row => (
                <TableRow key={row.id}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.email}</TableCell>
                  <TableCell>{row.location}</TableCell>
                  <TableCell>{row.products}</TableCell>
                  <TableCell>{row.contract_expiration_date}</TableCell>
                  <TableCell>{row.sales_physical_store}</TableCell>
                  <TableCell>{row.sales_online_store}</TableCell>
                </TableRow>
                ))}
          </TableBody>
          </Table>
          <TablePages count={rows.length}/>
          </Loader>
     </Paper>
    </div>
    )
}

export default Promotions;