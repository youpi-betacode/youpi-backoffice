import React from 'react';
import { Table, TableBody, TableCell, TableHead, TableRow, Paper, TableSortLabel } from '@material-ui/core';
import { colors } from '../../../../constants';
import Searchbar from '../common/searchbar';
import TablePages from '../tablePagination';
import { useSelector } from 'react-redux';
import CustomButton from '../common/button';
import Loader from '../common/loading';

function createData(orderID, sale, date, hour, youperId, orderStatus, total, orderChannel) {
  return { orderID, sale, date, hour, youperId, orderStatus, total, orderChannel };
}

const rows = [
  createData("#456721", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Preparação', '35,60', 'Página Web'),
  createData("#456722", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Acabamento', '35,60', 'Página Web'),
  createData("#456723", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Embalamento', '35,60', 'Página Web'),
  createData("#456724", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Envio', '35,60', 'Página Web'),
  createData("#456725", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Confirmação de entrega', '35,60', 'Página Web'),
  createData("#456726", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Embalamento', '35,60', 'Página Web'),
  createData("#456727", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Preparação', '35,60', 'Página Web'),
  createData("#456728", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Preparação', '35,60', 'Página Web'),
  createData("#456729", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Envio', '35,60', 'Página Web'),
  createData("#456730", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Preparação', '35,60', 'Página Web'),
  createData("#456731", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Embalamento', '35,70', 'Página Web'),
  createData("#456732", 'Image', '22/04/1029', '18:00', 'Maria João Cardoso', 'Preparação', '35,60', 'Página Web'),
]

//todo: change onClick action when adding reduce
const handleOnClick = (e) => {
  e.preventDefault();
}

/*todo: length of the products is still passed as props
* to table pagination change when integrating with backend
*/
function OrdersList() {

  const nrRows = useSelector(state => state.tablePagination.nrRows);
  const page = useSelector(state => state.tablePagination.currentPage);

  return (
    <div style={{ width: '100%' }}>
      <Paper>
        <Searchbar title='Vendas'>
          <CustomButton title='Exportar Report' action={handleOnClick} />
        </Searchbar>
        <Loader showChildren={true}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  <TableSortLabel
                    children={'ID'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'VENDA'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'DATA'}
                  />
                </TableCell>
                <TableCell >
                  <TableSortLabel
                    children={'HORA'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'YOUPER ID'}
                  />
                </TableCell>
                <TableCell >
                  <TableSortLabel
                    children={'ESTADO DA ENCOMENDA'}
                  />
                </TableCell>
                <TableCell >
                  <TableSortLabel
                    children={'TOTAL DA ENCOMENDA'}
                  />
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    children={'CANAL DA ENCOMENDA'}
                  />
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.slice(page * nrRows, page * nrRows + nrRows).map(row => (
                <TableRow key={row.orderID}>
                  <TableCell>{row.orderID}</TableCell>
                  <TableCell>{row.sale}</TableCell>
                  <TableCell>{row.date}</TableCell>
                  <TableCell>{row.hour}</TableCell>
                  <TableCell>{row.youperId}</TableCell>
                  <TableCell style={styles.orderStatus}>{row.orderStatus}</TableCell>
                  <TableCell>{row.total}€</TableCell>
                  <TableCell>{row.orderChannel}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <TablePages count={rows.length} />
        </Loader>
      </Paper>
    </div>
  )
}

export default OrdersList;

const styles = {
  orderStatus: {
    color: colors.orderStatusColor
  }
}