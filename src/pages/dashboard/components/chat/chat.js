import React from 'react'
import Paper from '@material-ui/core/Paper';
import { colors } from '../../../../constants';
import { makeStyles } from '@material-ui/core/styles';

function createData(id, name, email, location, products, contract_expiration_date, sales_physical_store, sales_online_store, sales_total) {
    return { id, name, email, location, products, contract_expiration_date, sales_physical_store, sales_online_store, sales_total };
}

const rows = [
    createData(0, 'Henhica', 'henhica@pimbolin.com', 'Henhicolandia', 25, "20/12/2019", 250, 300, 550),
    createData(0, 'Shportin', 'shportin@tranquilidade.pt', 'WC', 25, "20/12/2020", 250, 300, 550),
]

function Chat() {
    const classes = useStyles();

    return (
        <Paper className={classes.root}>
           chat
        </Paper>
    )
}

export default Chat;

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        // marginTop: theme.spacing(3),
        // overflowX: 'auto',
        backgroundColor: 'transparent',
    },
    table: {
    },
    tableHeader: {
        backgroundColor: colors.transparent06,
        color: colors.white,        
    },
    tableHeaderCol: {
        color: colors.white,
        fontSize: 14,
        borderBottom: 0,
        textAlign: 'center'
    },
    tableRow: {
        '&:nth-of-type(even)': {
            backgroundColor: colors.transparent04,           
        },
        '&:nth-of-type(odd)': {
            backgroundColor: colors.transparent02,
        },      
    },
    tableCol: {
        borderBottom: 0,
        textAlign: 'center'
    }
}));