import React from 'react';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import { useSelector, useDispatch } from 'react-redux'
import { rowsPerPage, currentPage } from "../../../redux/actions/tablePaginationActions";

export default function TablePages(props) {
    const nrRows = useSelector(state => state.tablePagination.nrRows);
    const page = useSelector(state => state.tablePagination.currentPage);

    const dispatch = useDispatch();

    const handleChangePage = (event, newPage) => {
      dispatch(currentPage(newPage));
       // setPage(newPage);
    }

   const handleChangeRowsPerPage = (event) => {
        dispatch(rowsPerPage(parseInt(event.target.value)));
        dispatch(currentPage(0));
        //setPage(0);
    };

  return (
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        colSpan={3}
        count={props.count}
        rowsPerPage={nrRows}
        page={page}
        SelectProps={{
          inputProps: { 'aria-label': 'Linhas por Página' },
          native: true,
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
        // ActionsComponent={TablePaginationActions}
      />
  );
}