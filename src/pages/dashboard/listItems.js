import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import HomeIcon from '@material-ui/icons/Home';
import SettingsIcon from '@material-ui/icons/Settings';
import Icon from '@material-ui/core/Icon';
import { makeStyles } from '@material-ui/core/styles';
import { colors, iconsSrc } from '../../constants';
import { Link } from 'react-router-dom';


export function MainListItems(props) {
  const classes = useStyles();
  return (
    <div>
      <ListItem button>
        <ListItemIcon>
          <HomeIcon className={classes.icon} />
        </ListItemIcon>
        <ListItemText primary="VISÃO GERAL" />
      </ListItem>
      <Link to='/orders'>
        <ListItem button>
          <img src={iconsSrc.stock} className={classes.iconPng} alt=''></img>
          {/* <ListItemIcon>
          <ShoppingCartIcon className={classes.icon}/>
        </ListItemIcon> */}
          <ListItemText primary="GESTÃO DE STOCK" className={classes.itemText} />
        </ListItem>
      </Link>

      <ListItem button>
        <img src={iconsSrc.sales} className={classes.iconPng} alt=''></img>
        {/* <ListItemIcon>
          <ShoppingCartIcon className={classes.icon}/>
        </ListItemIcon> */}
        <ListItemText primary="VENDAS" className={classes.itemText} />
      </ListItem>
      <ListItem button>
        <img src={iconsSrc.youpers} className={classes.iconPng} alt=''></img>
        {/* <ListItemIcon>
          <PeopleIcon className={classes.icon}/>
        </ListItemIcon> */}
        <ListItemText primary="YOUPERS" className={classes.itemText} />
      </ListItem>
      <ListItem button>
        <img src={iconsSrc.partners} className={classes.iconPng} alt=''></img>
        {/* <ListItemIcon>
          <BarChartIcon className={classes.icon}/>
        </ListItemIcon> */}
        <ListItemText primary="PARCEIROS" className={classes.itemText} />
      </ListItem>
      <ListItem button>
        <img src={iconsSrc.sponsors} className={classes.iconPng} alt=''></img>
        {/* <ListItemIcon>
          <LayersIcon className={classes.icon}/>
        </ListItemIcon> */}
        <ListItemText primary="SPONSORS" className={classes.itemText} />
      </ListItem>
      <ListItem button>
        <img src={iconsSrc.maillingList} className={classes.iconPng} alt=''></img>
        {/* <ListItemIcon>
          <LayersIcon className={classes.icon}/>
        </ListItemIcon> */}
        <ListItemText primary="MAILLING LIST" className={classes.itemText} />
      </ListItem>
      <ListItem button>
        <img src={iconsSrc.promotions} className={classes.iconPng} alt=''></img>
        {/* <ListItemIcon>
          <LayersIcon className={classes.icon}/>
        </ListItemIcon> */}
        <ListItemText primary="PROMOÇÕES" className={classes.itemText} />
      </ListItem>
      <ListItem button>
        <img src={iconsSrc.statistics} className={classes.iconPng} alt=''></img>
        {/* <ListItemIcon>
          <LayersIcon className={classes.icon}/>
        </ListItemIcon> */}
        <ListItemText primary="DADOS ESTATÍSTICOS" className={classes.itemText} />
      </ListItem>
      <ListItem button>
        <img src={iconsSrc.chat} className={classes.iconPng} alt=''></img>
        {/* <ListItemIcon>
          <LayersIcon className={classes.icon}/>
        </ListItemIcon> */}
        <ListItemText primary="CHAT" className={classes.itemText} />
      </ListItem>
    </div>
  )
};

export function SecondaryListItems() {
  const classes = useStyles();
  return (
    <div>
      <ListSubheader inset className={classes.listSubHeader}>PÁGINA WEB</ListSubheader>
      <ListItem button>
        <Icon className={classes.secondaryListIcon}>settings</Icon>
        <ListItemText primary="Homepage" className={classes.itemText}/>
      </ListItem>
      <ListItem button>
        <Icon className={classes.secondaryListIcon}>settings</Icon>
        <ListItemText primary="Quem somos" className={classes.itemText}/>
      </ListItem>
      <ListItem button>
        <Icon className={classes.secondaryListIcon}>settings</Icon>
        <ListItemText primary="YOUPI! Now" className={classes.itemText}/>
      </ListItem>
      <ListItem button>
        <Icon className={classes.secondaryListIcon}>settings</Icon>
        <ListItemText primary="Catálogo" className={classes.itemText}/>
      </ListItem>
      <ListItem button>
        <Icon className={classes.secondaryListIcon}>settings</Icon>
        <ListItemText primary="Inspira-te" className={classes.itemText}/>
      </ListItem>
      <ListItem button>
        <Icon className={classes.secondaryListIcon}>settings</Icon>
        <ListItemText primary="Parceiros" className={classes.itemText}/>
      </ListItem>
      <ListItem button>
      <Icon className={classes.secondaryListIcon}>settings</Icon>
        <ListItemText primary="Contactos" className={classes.itemText}/>
      </ListItem>
      <ListItem button>
        <Icon className={classes.secondaryListIcon}>settings</Icon>
        <ListItemText primary="Settings" className={classes.itemText}/>
      </ListItem>
    </div>
  )
};

const useStyles = makeStyles(theme => ({
  icon: {
    color: colors.iconGreenish,
  },
  listSubHeader: {
    color: colors.primary
  },
  secondaryListIcon: {
    color: colors.secondaryListItems
  },
  iconPng: {
    fontSize: '24px',
    position: 'absolute'
  },
  itemText: {
    paddingLeft: '15px'
  }
}))
