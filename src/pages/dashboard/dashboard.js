import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import DashboardView from './dashboard-view';
import AppDrawer from '../../components/drawer/drawer';
import AppToolbar from '../../components/toolbar/toolbar';
import SnackBars from './components/common/snackbars';


function Dashboard({ history }) {
  const classes = useStyles();

  return (
      <div className={classes.root}>
        <CssBaseline />
        <AppToolbar></AppToolbar>
        <AppDrawer></AppDrawer>
        <main className={classes.content}>
          <DashboardView history={history}></DashboardView>
        </main>
        <SnackBars/>
      </div>
  );
}

export default Dashboard;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',

  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
}));