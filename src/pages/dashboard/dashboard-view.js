import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { colors } from '../../constants';
import { connect } from 'react-redux';

import AppRouter from '../../navigation/appRouter';

function DashboardView({history}) {
  const classes = useStyles();

  return (
    <div>
      <div className={classes.appBarSpacer} />
      <Container maxWidth="lg" className={classes.container}>
        <Grid container>
          <AppRouter history={history}/>
        </Grid>
      </Container>
    </div>

  );
}

export default connect()(DashboardView)


const useStyles = makeStyles(theme => ({
  appBarSpacer: theme.mixins.toolbar,
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    backgroundColor: colors.transparent04
  },
  fixedHeight: {
    height: 240,
  },
}));