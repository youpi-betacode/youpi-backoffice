import React from 'react';
import { makeStyles, Grid, TextField, Link, Button } from '@material-ui/core';
import { FormControlLabel, Checkbox, Hidden } from '@material-ui/core';
import CheckboxIcon from '@material-ui/icons/RadioButtonUnchecked';
import CheckedIcon from '@material-ui/icons/RadioButtonChecked';
import { useSelector, useDispatch} from 'react-redux';

import { iconsSrc, colors } from '../../constants'
import { setEmail, setPassword, login } from '../../redux/actions/loginActions';


function Login() {
  const classes = useStyles();

  const email = useSelector(state => state.login.email)
  const password = useSelector(state => state.login.password);
  const dispatch = useDispatch();

  const handleKeyPress = (e) => {
    if(e.key === 'Enter')
      dispatch(login(email, password));
  }

  const handleChangeEmail = (e) => {
    dispatch(setEmail(e.target.value));
  }

  const handleChangePassword = (e) => {
    dispatch(setPassword(e.target.value))
  }

  const handleSubmitClick = (e) => {
    dispatch(login(email,password));
  }

  return (
    <div className={classes.alignItemsMiddle}>
      <Grid container>
        <Hidden smDown>
          <Grid item md={3}>
            <div>
              <img src={iconsSrc.left_lat} alt=' ' style={{ width: '100%' }} />
            </div>
          </Grid>
        </Hidden>
        <Grid item xs={12} md={6} style={{textAlign: 'center'}}>
          <div style={{textAlign: 'center'}}>
            <div>
              <img src={iconsSrc.youpi_login} alt='YOUPI' />
            </div>
            <div>
              <TextField
                onKeyUp={handleKeyPress}
                onChange={(e) => handleChangeEmail(e)}
                variant="outlined"
                margin="normal"
                required
                id="email"
                label='Email'
                name="email"
                autoComplete="off"
                color={colors.white}
                InputLabelProps={{
                  classes: {
                    root: classes.cssLabel,
                  }
                }}
                InputProps={{
                  classes: {
                    root: classes.cssOutLinedInput,
                    focused: classes.cssFocused,
                    notchedOutline: classes.notchedOutline
                  }
                }}
                className={classes.inputStyle}
              />
            </div>
            <div>
                <TextField
                  onKeyUp={handleKeyPress}
                  onChange={(e) => handleChangePassword(e)}
                  variant="outlined"
                  margin="normal"
                  required
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssFocused,
                    }
                }}
                InputProps={{
                  classes: {
                    root: classes.cssOutLinedInput,
                    focused: classes.cssFocused,
                    input: classes.cssFocused,
                    notchedOutline: classes.notchedOutline
                  }
                }}
                className={classes.inputStyle}
                />
            </div>
            <div>
              <Link href="#" variant="body2" style={{color:colors.loginGrey}}>
                {"Esqueceu a password?"}
              </Link>
            </div>
            <div>
              <Button
                className={classes.submit}
                onClick={(e) => handleSubmitClick(e)}
                type="submit"
                variant="contained"
                color='primary'
              >
                Login
              </Button>
            </div>
            <div style={{color:colors.loginGrey}}>
              <FormControlLabel
                control={<Checkbox value="remember"
                icon={<CheckboxIcon/>}
                checkedIcon={<CheckedIcon/>}
                />}
                label="Manter-me ligado"
              />
            </div>
          </div>
        </Grid>
        <Hidden smDown>
          <Grid item md={3}>
            <div>
              <img src={iconsSrc.right_lat} alt=' ' style={{ width: '100%' }} />
            </div>
          </Grid>
        </Hidden>
      </Grid>
    </div>
  )
}

export default Login;

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundImage: "url('/images/background.jpg')",
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat'
    }
  },
  alignItemsMiddle: {
    position: 'relative',
    top: '50%',
    transform: 'translateY(50%)'
  },
  submit: {
    '&:hover': {
      backgroundColor: colors.loginButtonHover
    },
    margin: theme.spacing(3, 0, 2),
    backgroundColor: colors.loginButton,
    textTransform: 'capitalize',
    borderRadius: 28,
    paddingRight: 40,
    paddingLeft: 40,
    color: colors.black
  },
  cssLabel: {
    color: colors.white + '!important',
    textAlign: 'left',
  },
  cssFocused: {
    color: colors.white + '!important',
  },
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: `${theme.palette.primary.main} !important`
    }
  },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: colors.white + ' !important',
    borderRadius: 28
  },
  inputStyle: {
    width: 300
  }
}))